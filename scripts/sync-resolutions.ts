import fs from 'node:fs/promises';
import jsonc from 'jsonc-parser';

const jsonTextIn = await fs.readFile( './package.json', { encoding: 'utf-8' } );
const data = jsonc.parse( jsonTextIn );

const versions = { ...( data.devDependencies ?? {} ), ...( data.dependencies ?? {} ) };

let jsonTextOut = jsonTextIn;
for( const [ dep, oldVer ] of Object.entries( data.resolutions ?? {} ) ) {
	const newVer = versions[ dep ];
	if( newVer != null && newVer !== oldVer ) {
		console.log( `${dep} ${oldVer} -> ${newVer}` );
		const edit = jsonc.modify( jsonTextOut, [ 'resolutions', dep ], newVer, {} );
		jsonTextOut = jsonc.applyEdits( jsonTextOut, edit );
	}
}

if( jsonTextOut !== jsonTextIn ) {
	fs.writeFile( './package.json', jsonTextOut, { encoding: 'utf-8' } );
}
