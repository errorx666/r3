import webpack from 'webpack';
import cluster, { Worker } from 'cluster';
import { getBuildId, getConfigurationFactories } from '../webpack.config.js';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import { from, fromEventPattern, merge } from 'rxjs';
import { mergeMap, take, tap } from 'rxjs/operators';
import type { EventEmitter } from 'events';

const fromNodeEvent = <T>( target: EventEmitter, event: string ) =>
	fromEventPattern<T>(
		e => { target.addListener( event, e as any ); },
		e => { target.removeListener( event, e as any ); }
	);

( async () => {
	let x =
		await yargs( hideBin( process.argv ) )
		.options( {
			p: {
				aliases: [ 'production', 'prod' ],
				type: 'boolean'
			},
			d: {
				aliases: [ 'development', 'dev' ],
				type: 'boolean'
			},
			h: {
				aliases: [ 'hot' ],
				type: 'boolean'
			},
			w: {
				aliases: [ 'watch' ],
				type: 'boolean'
			}
		} ).argv;
	let { production, development, hot, watch } = x;
	if( development ) production = false;
	production ??= !( watch ?? false );
	watch ??= false;
	hot ??= !production && watch;

	console.log( { x, production, watch, hot } );

	const configs = await getConfigurationFactories( { production }, { watch, hot } );
	if( cluster.isPrimary ) {
		console.log( { _: hideBin( process.argv ), production, watch, hot } );
		let workers = [] as readonly Worker[];
		for( let i = 0; i < configs.length; ++i ) {
			const worker =
				cluster.fork( {
					BUILD_ID: getBuildId(),
					NODE_ENV: production ? 'production' : 'development',
					INDEX: String( i )
				} );
			workers = [ ...workers, worker ];
		}

		from( workers )
		.pipe(
			mergeMap( ( worker, i ) =>
				merge(
					fromNodeEvent( worker, 'disconnect' )
					.pipe( tap( () => {
						console.log( `worker ${i} disconnect` );
					} ) ),
					fromNodeEvent( worker, 'exit' )
					.pipe( tap( ( [ , code, signal ] ) => {
						if( code != null && code !== 0 ) throw new Error( `process exited with code ${code}` );
						if( signal != null ) throw new Error( `process exited from signal ${signal}` );
						console.log( `worker ${i} exit` );
					} ) )
				)
				.pipe( take( 1 ) )
			)
		).subscribe( {
			complete() {
				cluster.disconnect();
				process.exit( 0 );
			},
			error() {
				cluster.disconnect();
				process.exit( 1 );
			}
		} );
	} else {
		const index = Number( process.env.INDEX );
		const config = await configs[ index ]();
		const compiler = webpack( config );

		function close( exitCode: number ) {
			compiler.close( closeErr => {
				if( closeErr != null ) {
					process.stderr.write( closeErr.toString() );
					exitCode = 1;
				}
				process.exit( exitCode );
			} );
		}

		function callback( { runOnce }: { readonly runOnce: boolean } ) {
			return ( err: Error, stats: webpack.Stats ) => {
				let exitCode = 0;
				if( err != null ) {
					process.stderr.write( err.toString() + '\n' );
					exitCode = 1;
					if( stats == null ) {
						if( runOnce ) {
							close( exitCode );
						}
						return;
					}
				}
				if( stats.hasErrors() ) {
					process.stderr.write( stats.toString( 'errors-only' ) + '\n' );
					exitCode = 1;
				} else {
					process.stdout.write( stats.toString( config.stats ) + '\n' );
					exitCode = 0;
				}
				if( runOnce ) {
					close( exitCode );
				}
			};
		}

		if( watch ) {
			const watcher = compiler.watch( {}, callback( { runOnce: false } ) );
			process.on( 'beforeExit', () => {
				watcher.close( closeErr => {
					let exitCode = 0;
					if( closeErr != null ) {
						process.stderr.write( closeErr.toString() + '\n' );
						exitCode = 1;
					}
					close( exitCode );
				} );
			} );
		} else {
			compiler.run( callback( { runOnce: true } ) );
		}
	}
} )();
