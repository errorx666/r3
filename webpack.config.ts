import crypto from 'crypto';
import fs from 'fs';
import path from 'path';
import nodeExternals from 'webpack-node-externals';
import webpack, { Configuration } from 'webpack';
import _ from 'lodash';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { AngularWebpackPlugin } from '@ngtools/webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import ScriptExtHtmlWebpackPlugin from 'script-ext-html-webpack-plugin';
import HtmlWebpackInlineSourcePlugin from 'html-webpack-inline-source-plugin';
// import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import CleanObsoleteChunksPlugin from 'webpack-clean-obsolete-chunks';
import PreloadWebpackPlugin from 'preload-webpack-plugin';
// import CopyWebpackPlugin from 'copy-webpack-plugin';
import { v4 as uuid } from 'uuid';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import upath from 'upath';
import glob from 'fast-glob';

import { AutostartWebpackPlugin } from '@errorx666/autostart-webpack-plugin';

import linkerPlugin from '@angular/compiler-cli/linker/babel';

import yaml from 'js-yaml';
const __dirname = process.cwd();

// function externals( { context, request }, callback ) {
// 	if( request === 'three' ) {
// 		callback( null, 'THREE', 'root' );
// 		return;
// 	}
// 	if( /three/i.test( request ) ) {
// 		const r = upath.relative( upath.resolve( __dirname, 'node_modules' ), upath.resolve( context, request ) );
// 		if( r === 'three/build/three.module.js' ) {
// 			callback( null, 'THREE', 'root' );
// 			return;
// 		}
// 	}
// 	callback();
// }

export function getBuildId() {
	const { BUILD_ID } = getBuildId;
	// console.log( { BUILD_ID } );
	return BUILD_ID;
}
export namespace getBuildId {
	export const BUILD_ID = process.env.BUILD_ID ?? crypto.createHash( 'md5' ).update( uuid() ).digest( 'base64' ).replace( /=+$/, '' ).slice( 0, 6 );
}

export interface Param1 {
	readonly production?: boolean;
}
export interface Param2 {
	readonly watch?: boolean;
	readonly hot?: boolean;
}

export async function getConfigurationFactories( env = {} as Param1, settings = {} as Param2 ) {
	const isProduction = !!( env?.production ?? ( process.env.NODE_ENV === 'production' ) );
	const isHot = !!settings?.hot;
	const isWatch = !!settings?.watch;
	const config = yaml.load( await fs.promises.readFile( path.resolve( 'webpack.yaml' ) ) );
	const devMode = isProduction ? false : config.devMode;
	const mode = devMode ? 'development' : 'production';

	console.log( { devMode, mode, isProduction, isHot, isWatch } );

	// const threeHash = crypto.createHash( 'md5' ).update( await fs.promises.readFile( threePath ) ).digest( 'hex' );
	// const threeName = `three-${threeHash}.js`;
	const profile = false;

	// const { default: linkerPlugin } = await import( './node_modules/@angular/compiler-cli/bundles/linker/babel/index.js' );

	const loader = {
		angular: { loader: '@ngtools/webpack', options: { tsconfig: path.resolve( __dirname, 'tsconfig.client.json' ) } },
		angularLinker: { loader: 'babel-loader' },
		babelClient: { loader: 'babel-loader' },
		babelServer: { loader: 'babel-loader' },
		css: { loader: 'css-loader' },
		debug: { loader: 'debug-loader' },
		glsl: { loader: 'webpack-glsl-loader' },
		html: { loader: 'html-loader' },
		raw: { loader: 'raw-loader' },
		null: { loader: 'null-loader' },
		postcss: { loader: 'postcss-loader' },
		style: { loader: MiniCssExtractPlugin.loader },
		text: { loader: 'text-loader' },
		typescript: { loader: 'ts-loader' },
		yaml: { loader: 'js-yaml-loader' }
	} as Record<string, { loader: string, options?: any }>;

	for( const [ key, obj ] of Object.entries( loader ) ) {
		if( !obj.options ) obj.options = config.options[ key ] || {};
	}

	loader.angularLinker.options.plugins = [ linkerPlugin ];

	const esVersions = [ 'next', ...( function *() {
		for( let year = ( new Date ).getFullYear(); year >= 2015; --year ) {
			yield String( year );
		}
	}() ), '6' ];
	const esTypes = [ 'fesm', 'fes', 'esm', 'es', 'jsm', 'js' ];

	const resolve = {
		extensions: [ '.ts', '.js', '.json' ],
		mainFields: [ 'browser', ...esTypes.flatMap( esType => esVersions.map( esVersion => `${esType}${esVersion}` ) ), 'module', 'main' ]
	};

	const angularPattern = /\.(?:ng)?(?:component|directive|factory|pipe|module|service|style)\./i;

	const buildId = getBuildId();

	const define = {
		BUILD_ID: JSON.stringify( buildId )
	};

	const environment = {
		DEBUG: String( devMode ),
		NODE_ENV: mode
	};

	const clientConfig: () => Promise<Configuration> = async () => _.merge( {}, config.configuration.shared, config.configuration.client, { mode, resolve }, {
		name: 'client',
		entry: {
			client: path.resolve( __dirname, 'src', 'client', 'main' )
		},
		// externals,
		module: {
			rules: [
				{ test: /\.(?:vert|tesc|tese|geom|frag|comp|glsl)$/i, use: [ loader.glsl ] },
				{ test: /\.html$/i, use: [ loader.raw ] },
				{ test: /\.s?css$/i, include: [ path.resolve( __dirname, 'src', 'client' ) ], oneOf: [
					{ test: angularPattern, use: [ loader.text ] },
					{ use: [ loader.style, loader.css ] }
				] },
				{ test: /\.css$/i, use: [ loader.postcss ] },
				{ test: /\.(?:png|jpe?g|gif|svg|woff\d*|[ot]tf|eot)/i, type: 'asset/resource' },
				{ test: /\.ya?ml$/i, use: [ loader.yaml ] },
				// { test: /app\.module\.ts/i, include: [ path.resolve( __dirname, 'src' ) ], use: [
				// 	loader.debug
				// ] },
				{ test: /\.[mc]?[jt]s$/i, include: [ path.resolve( __dirname, 'src' ) ], use: [
					// ...( isProduction ? [ loader.optimizer ] : [] ),
					loader.babelClient
				] },
				{ test: /\.[mc]?[jt]s$/i,
					use: [ loader.angularLinker ],
					exclude: [ path.resolve( __dirname, 'src', 'client', 'workers' ) ]
				},
				{ test: /\.ts$/i,
					use: [ loader.angular ],
					exclude: [ path.resolve( __dirname, 'src', 'client', 'workers' ) ]
				},
				// { test: require.resolve( 'core-js/modules/es.promise' ), use: [ loader.null ] }
			]
		},
		output: {
			path: path.resolve( __dirname, ...config.configuration.client.output.path )
		},
		plugins: [
			new AngularWebpackPlugin( { tsconfig: path.resolve( __dirname, 'tsconfig.client.json' ) } ),
			new MiniCssExtractPlugin( { filename: '[contenthash].css' } ),
			new webpack.DefinePlugin( {
				...define,
				'process.browser': JSON.stringify( true ),
				'process.platform': JSON.stringify( 'browser' ),
				IS_WORKER: JSON.stringify( false )
			} ),
			new webpack.EnvironmentPlugin( environment ),
			new HtmlWebpackPlugin( {
				favicon: path.resolve( __dirname, 'assets', 'logo.png' ),
				inject: 'body',
				inlineSource: /(?:^[a-f0-9]{20}\.js|\.css)/,
				template: path.resolve( __dirname, 'src', 'client', 'index.ejs' ),
				templateParameters: {
					...config.templateParameters,
					buildId,
					// threeName,
					hash: async file =>
						crypto.createHash( 'md5' )
						.update( await fs.promises.readFile( path.join( __dirname, file ) ) )
						.digest( 'hex' )
				}
			} ),
			// new CopyWebpackPlugin( {
			// 	patterns: [
			// 		{
			// 			from: threePath,
			// 			to: threeName
			// 		}
			// 	]
			// } ),
			new PreloadWebpackPlugin( {
				include: 'allAssets',
				rel: 'prefetch',
				fileBlacklist: [ /icons-/, /\.(?:css|json|map|xml)/, /^[a-f0-9]{20}\.js/ ],
				as: source =>
					/\.(?:mp3|opus|wav)/i.test( source ) ? 'audio'
				:	/\.(?:glb|gltf|glsl|vert|tesc|tese|geom|frag|comp)/i.test( source ) ? 'fetch'
				:	/\.(?:eof|otf|ttf|woff\d*)/i.test( source ) ? 'font'
				:	/\.(?:gif|jpe?g|png)/i.test( source ) ? 'image'
				:	/\.(?:js)/i.test( source ) ? 'script'
				:	/\.(?:css)/i.test( source ) ? 'style'
				:	null
			} ),
			new ScriptExtHtmlWebpackPlugin( {
				defaultAttribute: 'async'
			} ),
			new ( HtmlWebpackInlineSourcePlugin as any )( HtmlWebpackPlugin ),
			...( devMode ? [
				new CleanObsoleteChunksPlugin
			] : [
				// new BundleAnalyzerPlugin( {
				// 	analyzerMode: 'static',
				// 	reportFilename: path.resolve( __dirname, 'stats', 'client.html' )
				// } )
			] ),
			...( profile ? [
				new webpack.debug.ProfilingPlugin( {
					outputPath: path.resolve( __dirname, 'profile', 'client.json' )
				} )
			] : [] )
		],
		resolve: {
			plugins: [
				new TsconfigPathsPlugin( { configFile: path.resolve( __dirname, 'tsconfig.client.json' ) } )
			]
		}
	} as Configuration );

	const workersConfig: () => Promise<Configuration> = async () => _.merge( {}, config.configuration.shared, config.configuration.workers, { mode, resolve }, /** @type {webpack.Configuration} */ {
		name: 'workers',
		entry: {
			...Object.fromEntries( glob.sync( [ 'src/client/workers/**/*.worker.ts' ] ).map( f => [
				upath.relative( upath.resolve( __dirname, 'src', 'client' ), upath.trimExt( f, [ 'worker' ] ) ),
				[
					path.resolve( __dirname, ...upath.normalize( f ).split( '/' ) )
				]
			] ) )
		},
		module: {
			rules: [
				{ test: /\.(?:vert|tesc|tese|geom|frag|comp|glsl)$/i, use: [ loader.glsl ] },
				{ test: /\.ya?ml$/i, use: [ loader.yaml ] },
				{ test: /\.[jt]s$/i, include: [ path.resolve( __dirname, 'src' ) ], use: [ loader.babelClient ] },
				{ test: /\.ts$/i, exclude: [ /\.(?:component|directive|module|pipe|service)\.ts$/i ], use: [ _.merge( {}, loader.typescript, { options: { configFile: 'tsconfig.workers.json' } } ) ] },
				// { test: require.resolve( 'core-js/modules/es.promise' ), use: [ loader.null ] }
			]
		},
		output: {
			path: path.resolve( __dirname, ...config.configuration.workers.output.path )
		},
		plugins: [
			new webpack.DefinePlugin( {
				...define,
				'process.browser': JSON.stringify( true ),
				'process.platform': JSON.stringify( 'browser' ),
				IS_WORKER: JSON.stringify( true )
			} ),
			new webpack.EnvironmentPlugin( environment ),
			// new webpack.EnvironmentPlugin( {
			// 	...environment,
			// 	THREE_NAME: threeName
			// } ),
			...( devMode ? [] : [
				// new BundleAnalyzerPlugin( {
				// 	analyzerMode: 'static',
				// 	reportFilename: path.resolve( __dirname, 'stats', 'workers.html' )
				// } )
			] ),
			...( profile ? [
				new webpack.debug.ProfilingPlugin( {
					outputPath: path.resolve( __dirname, 'profile', 'workers.json' )
				} )
			] : [] )
		],
		// externals,
		resolve: {
			alias: {},
			plugins: [
				new TsconfigPathsPlugin( { configFile: path.resolve( __dirname, 'tsconfig.workers.json' ) } )
			]
		}
	} as Configuration );

	const serverConfig: () => Promise<Configuration> = async () => _.merge( {}, config.configuration.shared, config.configuration.server, { mode, resolve }, /** @type {webpack.Configuration} */ {
		name: 'server',
		entry: {
			server: [
				'source-map-support/register',
				path.resolve( __dirname, 'src', 'server', 'start' )
			]
		},
		module: {
			rules: [
				{ test: /\.ya?ml$/i, use: [ loader.yaml ] },
				{ test: /\.[jt]s$/i, include: [ path.resolve( __dirname, 'src' ) ], use: [ loader.babelServer ] },
				{ test: /\.ts$/i, use: [ _.merge( {}, loader.typescript, { options: { configFile: 'tsconfig.server.json' } } ) ] },
			]
		},
		output: {
			devtoolModuleFilenameTemplate: '[absolute-resource-path]',
			path: path.resolve( __dirname, ...config.configuration.server.output.path )
		},
		optimization: {
			minimize: false
		},
		externals: [
			nodeExternals( {
				modulesFromFile: {
					exclude: [ 'devDependencies' ]
				}
			} )
		],
		plugins: [
			new webpack.DefinePlugin( { ...define } ),
			new webpack.EnvironmentPlugin( environment ),
			...( isWatch ? [
				new AutostartWebpackPlugin( {
					modulePath: './dist/server.cjs',
					env: {},
					cwd: __dirname,
					args: [],
					nodeArgs: [ '--inspect=0.0.0.0:9229' ],
					kill: !isHot
				} )
			] : [] ),
			...( devMode ? [] : [
				// new BundleAnalyzerPlugin( {
				// 	analyzerMode: 'static',
				// 	reportFilename: path.resolve( __dirname, 'stats', 'server.html' )
				// } )
			] ),
			...( profile ? [
				new webpack.debug.ProfilingPlugin( {
					outputPath: path.resolve( __dirname, 'profile', 'server.json' )
				} )
			] : [] )
		],
		resolve: {
			plugins: [
				new TsconfigPathsPlugin( { configFile: path.resolve( __dirname, 'tsconfig.server.json' ) } )
			]
		}
	} as Configuration );

	const configurationFactories = [ clientConfig, workersConfig, serverConfig ];
	// for( const configurationFactory of configurationFactories ) {
	// 	console.log( configurationFactory() );
	// }
	return configurationFactories;
}

export default async function getAllConfigurations( param1: Param1, param2: Param2 ) {
	return Promise.all( ( await getConfigurationFactories( param1, param2 ) ).map( async c => await c() ) );
}
