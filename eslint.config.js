import js from '@eslint/js';
import globals from 'globals';
import eslintPluginJsonc from 'eslint-plugin-jsonc';
import typescriptParser from '@typescript-eslint/parser';
import typescriptEslintPlugin from '@typescript-eslint/eslint-plugin';

export default [
	js.configs.recommended,
	...eslintPluginJsonc.configs[ 'flat/recommended-with-json' ],
	{
		files: [ '.vscode/**/*.json' ],
		rules: {
			'jsonc/no-comments': 'off'
		}
	},
	{
		ignores: [
			'assets/**/*',
			'dist/**/*',
			'log/**/*',
			'node_modules/**/*',
			'profile/**/*',
			'src/data/**/*',
			'stats/**/*',
			'.cache/**/*'
		]
	}, {
		rules: {
			'array-bracket-spacing': [
				'warn',
				'always'
			],
			'arrow-spacing': 'error',
			'block-spacing': 'error',
			camelcase: 'off',
			'consistent-this': [
				'error',
				'self'
			],
			'computed-property-spacing': [
				'warn',
				'always'
			],
			'comma-spacing': 'warn',
			'eol-last': 'error',
			'generator-star-spacing': 'error',
			'id-blacklist': [
			  'error',
			  'foo',
			  'bar',
			  'baz',
			  'qux',
			  'quux',
			  'that'
			],
			indent: 'off',
			quotes: [
				'error',
				'single',
				{ avoidEscape: true,
				allowTemplateLiterals: true
				}
			],
			'keyword-spacing': 'off',
			'new-cap': [ 'warn', {
				capIsNewExceptions: [
					'Component',
					'Directive',
					'HostBinding',
					'HostListener',
					'Injectable',
					'Input',
					'NgModule',
					'Output',
					'Pipe',
					'Self',
					'ViewChild',
					'ViewChildren'
				]
			} ],
			'no-array-constructor': 'error',
			'no-async-promise-executor': 'off',
			'no-catch-shadow': 'error',
			'no-console': 'off',
			'no-empty': 'off',
			'no-empty-pattern': 'off',
			'no-extra-boolean-cast': 'warn',
			'no-inner-declarations': 'off',
			'no-label-var': 'error',
			'no-lonely-if': 'off',
			'no-multi-spaces': 'off',
			'no-negated-condition': 'warn',
			'no-new-func': 'error',
			'no-new-symbol': 'error',
			'no-new-wrappers': 'error',
			'no-proto': 'error',
			'no-script-url': 'error',
			'no-self-compare': 'error',
			'no-sequences': 'error',
			'no-spaced-func': 'error',
			'no-shadow-restricted-names': 'error',
			'no-this-before-super': 'error',
			'no-throw-literal': 'error',
			'no-trailing-spaces': 'error',
			'no-undef-init': 'error',
			'no-unused-expressions': 'error',
			'no-unused-vars': 'warn',
			'no-use-before-define': 'warn',
			'no-useless-call': 'warn',
			'no-useless-concat': 'error',
			'no-useless-escape': 'warn',
			'no-useless-computed-key': 'error',
			'no-useless-constructor': 'off',
			'no-useless-rename': 'error',
			'no-var': 'error',
			'no-with': 'error',
			'no-whitespace-before-property': 'error',
			'object-curly-spacing': 'off',
			'object-shorthand': 'error',
			'one-var': 'off',
			'one-var-declaration-per-line': 'off',
			'key-spacing': 'error',
			'padded-blocks': 'off',
			'prefer-const': 'off',
			'prefer-rest-params': 'error',
			'quote-props': [
				'warn',
				'as-needed'
			],
			radix: 'error',
			'require-yield': 'error',
			'rest-spread-spacing': 'error',
			semi: 'error',
			'semi-spacing': 'error',
			'space-before-blocks': [
				'error',
				'always'
			],
			'space-before-function-paren': 'off',
			'space-in-parens': 'off',
			'space-infix-ops': 'warn',
			'space-unary-ops': 'warn',
			'spaced-comment': 'off',
			'template-curly-spacing': 'error',
			'unicode-bom': 'error',
			'wrap-iife': 'error',
			'yield-star-spacing': 'off',
			'react/display-name': 'off',
			'react/react-in-jsx-scope': 'off',
			'react/prop-types': 'off'
		}
	},
	{
		files: [ '*.{m,c,}js' ],
		languageOptions: {
			globals: {
				...globals.node
			}
		}
	},
	{
		files: [
			'src/**/*.ts'
		],
		ignores: [
			'src/**/*.d.ts'
		],
		plugins: {
			'@typescript-eslint': typescriptEslintPlugin
		},
		languageOptions: {
			parser: typescriptParser,
			parserOptions: {
				project: './tsconfig.json',
				sourceType: 'module',
				ecmaVersion: 2024
			},
			globals: {
				...globals.node
			}
		},
		rules: {
			'@typescript-eslint/ban-types': 'off',
			'no-dupe-class-members': 'off',
			'no-redeclare': 'off',
			'no-use-before-define': 'off',
			'no-unused-expressions': 'off',
			'no-unused-vars': 'off',
			'no-undef': 'off',
			'@typescript-eslint/no-unused-expressions': 'error',
			'@typescript-eslint/explicit-function-return-type': 'off',
			'@typescript-eslint/prefer-interface': 'off',
			'@typescript-eslint/no-explicit-any': 'off',
			'@typescript-eslint/no-namespace': 'off',
			'@typescript-eslint/no-object-literal-type-assertion': 'off',
			'@typescript-eslint/no-parameter-properties': 'off',
			'@typescript-eslint/no-unused-vars': [
				'warn',
				{ args: 'none',
				  varsIgnorePattern: '^_'
				}
			],
			'@typescript-eslint/no-use-before-define': [
				'warn',
				'nofunc'
			]
		}
	} ];
