import { Collection, Db, MongoClient } from 'mongodb';
import { url, clientOptions, dbOptions } from '~data/mongodb.config.yaml';
import { onShutDown } from './shut-down';
import schema from '~data/server.schema.yaml';

let connection: Promise<{
	collections: {
		expirations: Collection<MongoExpiration>;
		games: Collection<MongoGame>;
		rooms: Collection<MongoRoom>;
		roomSessions: Collection<MongoRoomSession>;
		sessions: Collection<MongoSession>;
		users: Collection<MongoUser>;
	},
	client: MongoClient,
	db: Db
}>;

export function connectMongodb() {
	if( !connection ) connection = ( async () => {
		const client = await MongoClient.connect( process.env.MONGODB_URI || url, clientOptions );
		const db = client.db( undefined, dbOptions );

		const collectionNames = new Set<string>( ( await db.listCollections( {}, { nameOnly: true } ).toArray() ).map( ( { name } ) => name ) );
		const validationAction = 'warn';
		const getCollection = async <T> ( name: string ) => {
			if( collectionNames.has( name ) ) return db.collection<T>( name );
			const validator = schema[ name ];
			return db.createCollection<T>( name, { validator, validationAction } );
		};

		const collections = {
			expirations: await getCollection<MongoExpiration>( 'expiration' ),
			games: await getCollection<MongoGame>( 'game' ),
			rooms: await getCollection<MongoRoom>( 'room' ),
			roomSessions: await getCollection<MongoRoomSession>( 'roomSession' ),
			sessions: await getCollection<MongoSession>( 'sessions' ),
			users: await getCollection<MongoUser>( 'user' )
		};
		onShutDown( () => client.close() );
		return { db, client, collections };
	} )();
	return connection;
}
