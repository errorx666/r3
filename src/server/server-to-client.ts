export function s2cRoom( { _id, name, passwordHash, gameId }: ServerRoom ): ClientRoom {
	return { id: _id.toHexString(), name, hasPassword: passwordHash?.buffer.byteLength > 0, gameId: gameId?.toHexString() };
}

export function s2cGame( { _id, colors, ruleSet, gameStates }: ServerGame ): ClientGame {
	return { id: _id.toHexString(), colors: [ ...colors ], ruleSet, gameStates: gameStates.map( s2cGameState ) };
}

export function s2cGameState( { time, turn, lastMove, size, data }: ServerGameState ): ClientGameState {
	return { time: time.toISOString(), turn, lastMove, size, data: [ ...data ] };
}

export function s2cRoomSession( { roomId, sessionId, seats }: ServerRoomSession ) {
	return { roomId: roomId.toHexString(), sessionId: sessionId.toHexString(), seats: [ ...seats ] };
}

export function s2cSession( { _id, nick }: ServerSession ) {
	return { id: _id.toHexString(), nick };
}
