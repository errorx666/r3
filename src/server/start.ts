import './error-handler';
import './polyfills';

import path from 'path';

import log4js from 'log4js';

const isProduction = process.env.NODE_ENV === 'production';

const logDir = path.resolve( __dirname, '..', 'log' );
const layout = {
	type: 'pattern',
	pattern: '%d %p %c %f:%l %m%n'
};

function logFile( filename: string ) {
	return {
		type: 'dateFile',
		filename: path.resolve( logDir, filename ),
		keepFileExt: true,
		numBackups: 30,
		pattern: 'yyyy-MM-dd',
		encoding: 'utf-8',
		layout
	};
}

log4js.configure( {
	appenders: {
		console: {
			type: 'console',
			layout
		},
		stderr: {
			type: 'stderr',
			layout
		},
		stdout: {
			type: 'stdout',
			layout
		},
		master: logFile( 'master.log' ),
		process: logFile( 'process.log' ),
		errors: logFile( 'errors.log' ),
		'console-warn': {
			type: 'logLevelFilter',
			appender: 'console',
			level: 'warn'
		},
		'only-errors': {
			type: 'logLevelFilter',
			appender: 'errors',
			level: 'error'
		}
	},
	categories: {
		default: {
			appenders: [ isProduction ? 'console-warn' : 'console', 'master', 'only-errors' ],
			level: 'info',
			enableCallStack: true
		},
		process: {
			appenders: [ 'console-warn', 'process', 'master', 'only-errors' ],
			level: 'debug',
			enableCallStack: true
		}
	}
} );

import( /* webpackChunkName: "main~server" */ './main' );
