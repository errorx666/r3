import { getLogger } from 'log4js';

const log = getLogger( 'process' );

process.on( 'uncaughtException', err => {
	log.fatal( `uncaughtException: ${err}\n${err.stack}` );
	process.exit( 1 );
} );

process.on( 'unhandledRejection', ( err: any = {} ) => {
	log.fatal( `unhandledRejection: ${err}\n${err.stack}` );
} );
