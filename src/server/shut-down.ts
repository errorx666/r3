import { ReplaySubject, of } from 'rxjs';
import { mergeMap, map, take, concatMap, switchMap } from 'rxjs/operators';
import { fromNodeEvent, logErrors } from './rxjs';
import log4js, { getLogger } from 'log4js';
import { promisify } from 'util';
const log = getLogger();

type ShutDownHook = () => void|PromiseLike<void>;

const shutDownFns = [] as ShutDownHook[];

const _shuttingDown = new ReplaySubject<true>( 1 );
export const shuttingDown = _shuttingDown.pipe( take( 1 ) );
export function shutDown() {
	_shuttingDown.next( true );
	_shuttingDown.complete();
}

export function onShutDown( fn: ShutDownHook ) {
	shutDownFns.push( fn );
}

of( 'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM' )
.pipe(
	mergeMap( sig =>
		fromNodeEvent( process, sig )
		.pipe( map( () => sig ) )
	)
)
.subscribe( sig => {
	log.info( `Received ${sig}` );
	shutDown();
} );

_shuttingDown
.pipe(
	take( 1 ),
	switchMap( () => [
		() => {
			log.info( 'Shutting down...' );
		},
		...shutDownFns,
		async () => {
			await promisify( log4js.shutdown ).call( log4js );
		}
	] ),
	concatMap( fn => Promise.resolve( fn() ) ),
	logErrors( log )
)
.subscribe( {
	complete: () => {
		process.exit( 0 );
	},
	error: err => {
		log.fatal( err );
		process.exit( 1 );
	}
} );
