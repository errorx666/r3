import { onShutDown, shuttingDown } from './shut-down';
import { server } from './app';
import { promisify } from 'util';
import { fromNodeEvent, logErrors } from '~server/rxjs';
import { filter, takeUntil, mergeMap, share } from 'rxjs/operators';
import { EventEmitter } from 'events';
import { Server } from 'socket.io';
import { connectMongodb } from './mongodb';
import { Observable } from 'rxjs';
import { getLogger } from 'log4js';
import { ObjectId } from 'mongodb';

const log = getLogger();

const isProduction = process.env.NODE_ENV === 'production';

export const io = new Server( server, {
	perMessageDeflate: isProduction,
	transports: [ 'websocket', 'polling' ],
	cookie: false
} );

fromNodeEvent( io.engine, 'connection_error' )
.subscribe( err => {
	log.error( err );
} );

onShutDown( async () => {
	try {
		await promisify( io.close ).call( io );
	} catch( err ) {
		log.error( err );
	}
} );

io.use( ( socket, cb ) => {
	( async () => {
		try {
			const socketId = socket.id;
			const sessionId = socket.handshake.query.sessionId ? new ObjectId( socket.handshake.query.sessionId as string ) : new ObjectId;
			log.info( `Connection from ${sessionId.toHexString()}` );
			const { collections } = await connectMongodb();
			await collections.sessions.findOneAndUpdate(
				{ _id: sessionId },
				{
					$set: { socketId },
					$setOnInsert: { id: sessionId, nick: 'Guest', userId: null }
				},
				{ upsert: true }
			);
			socket.join( sessionId.toHexString() );
		} catch( err ) {
			log.fatal( err );
			throw err;
		}
	} )().then( () => { cb(); }, cb );
} );

type EventTarget = Pick<NodeJS.EventEmitter|EventEmitter, 'addListener'|'removeListener'>;

type Message<T> = { type: string, data: T };
type MessageEventArgs<T> = [ Message<T>, Function ];
const messageHandlers = new WeakMap<EventTarget, Observable<MessageEventArgs<any>>>();

export function handleMessage<T>( target: EventTarget, type: string, handler: ( data: T ) => Promise<any>|void ) {
	let messages: Observable<MessageEventArgs<T>> = messageHandlers.get( target );
	if( !messages ) {
		messages =
			fromNodeEvent<MessageEventArgs<T>>( target, 'message' )
			.pipe(
				takeUntil( shuttingDown ),
				logErrors( log ),
				share()
			);
		messageHandlers.set( target, messages );
	}

	messages
	.pipe(
		takeUntil( shuttingDown ),
		filter( ( [ m ] ) => m.type === type ),
		mergeMap( async ( [ m, callback ] ) => {
			try {
				callback( null, await handler( m.data ) || {} );
			} catch( ex ) {
				log.error( ex );
				const error = ex?.message ?? 'Error';
				callback( { error }, null );
			}
		} ),
		logErrors( log )
	)
	.subscribe();
}
