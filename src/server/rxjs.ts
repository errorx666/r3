import { EventEmitter } from 'events';
import { fromEventPattern } from 'rxjs';
import { tap } from 'rxjs/operators';
import type { Logger } from 'log4js';

type EventTarget = Pick<NodeJS.EventEmitter|EventEmitter, 'addListener'|'removeListener'>;

export const fromNodeEvent = <T>( target: EventTarget, event: string ) =>
	fromEventPattern<T>(
		e => { target.addListener( event, e as any ); },
		e => { target.removeListener( event, e as any ); }
	);

export const logErrors = <T>( logger: Logger ) => tap<T>( { error( err ) {
	logger.fatal( err );
} } );
