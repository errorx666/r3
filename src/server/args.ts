import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

export const argv  =
	await yargs( hideBin( process.argv ) )
	.options( {} ).argv;
