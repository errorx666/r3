import './error-handler';
import './polyfills';

import { take, takeUntil, share, exhaustMap, switchMap, mergeMap, shareReplay, tap } from 'rxjs/operators';
import { fromNodeEvent } from './rxjs';
import { isValidRoomName, isValidNick, isValidUserPassword } from '~validation';
import { ruleSetMap } from '~rule-sets';
import { io, handleMessage } from './socket';
import { colors } from '~data/colors.yaml';
import { hashPassword, checkPassword } from './security';
import { s2cRoom, s2cGame } from './server-to-client';
import { s2mGame } from './server-to-mongo';
import { connectMongodb } from './mongodb';
import { Int32, ObjectId } from 'bson';
import { snoozeExpiry } from './cleanup';
import { timer } from 'rxjs';
import { localBus } from './bus';
import { c2sGameState } from './client-to-server';
import { c2mGameState } from './client-to-mongo';
import { clientSession$, clientRoom$, clientRoomSession$, serverSessionDelete$ } from './reactive-data';
import { m2cGame, m2cRoomSession } from './mongo-to-client';
import { m2sGame } from './mongo-to-server';
import { getLogger } from 'log4js';
import type { Socket } from 'socket.io';

const log = getLogger();

clientRoom$.pipe(
	switchMap( async data => {
		io.send( { type: 'rooms', data } );
	} )
)
.subscribe();

clientSession$.pipe(
	switchMap( async data => {
		io.send( { type: 'sessions', data } );
	} )
)
.subscribe();

clientRoomSession$.pipe(
	switchMap( async data => {
		io.send( { type: 'roomSessions', data } );
	} )
)
.subscribe();

( async () => {
	try {
		const { collections } = await connectMongodb();

		serverSessionDelete$
		.pipe(
			mergeMap( async session => {
				const { deletedCount } = await collections.roomSessions.deleteMany( { sessionId: session._id } );
				if( deletedCount > 0 ) {
					localBus.next( { type: BusMessageType.UpdateRoomSession, data: {} } );
				}
			} )
		)
		.subscribe();

		const getNick = async ( sessionId: ObjectId ) =>
			( await collections.sessions.findOne( { _id: sessionId }, { projection: { _id: 0, nick: 1 } } ) )?.nick;

		let connections = 0;

		fromNodeEvent<Socket>( io, 'connection' )
		.pipe( tap( () => {
			log.info( 'connection' );
		} ) )
		.subscribe( async socket => {
			try {
				const { _id: sessionId } = await collections.sessions.findOne( { socketId: socket.id } );
				log.info( `User connected, ${++connections} connected, ${sessionId}` );
				const sessionIdStr = sessionId.toHexString();

				io.to( sessionIdStr ).emit( 'message', { type: 'build', data: BUILD_ID } );

				{
					const roomIds = await collections.roomSessions.find( { sessionId } ).project( { _id: 0, roomId: 1 } ).map( r => r.roomId ).toArray() as readonly ObjectId[];
					for( const roomId of roomIds ) {
						socket.join( roomId.toHexString() );
					}

					io.to( sessionIdStr ).emit( 'message', { type: 'session', data: { sessionId: sessionIdStr } } );
					const roomSessions = await collections.roomSessions.find( {} ).map( rs => m2cRoomSession( rs as unknown as MongoRoomSession ) ).toArray();
					io.to( sessionIdStr ).emit( 'message', { type: 'roomSessions', data: roomSessions } );
					const rooms = await collections.rooms.find( {} ).map( s2cRoom ).toArray();
					io.to( sessionIdStr ).emit( 'message', { type: 'rooms', data: rooms } );

					const gameIds = ( await collections.rooms.find( { _id: { $in: roomIds } } ).project( { _id: 0, gameId: 1 } ).toArray() ).map( r => r.gameId );
					const games = await collections.games.find( { _id: { $in: gameIds } } ).toArray();
					for( const game of games.map( m2cGame ) ) {
						io.to( sessionIdStr ).emit( 'message', { type: 'update', data: game } );
					}
				}

				localBus.next( { type: BusMessageType.UpdateSession, data: {} } );

				const disconnected = fromNodeEvent( socket, 'disconnect' ).pipe( shareReplay( 1 ), take( 1 ) );
				const disconnecting = fromNodeEvent( socket, 'disconnecting' ).pipe( share(), take( 1 ), takeUntil( disconnected ) );

				timer( 0, 60 * 1000 )
				.pipe(
					exhaustMap( async () => {
						await snoozeExpiry( 1000 * 60 * 10, sessionId );
					} ),
					takeUntil( disconnecting )
				)
				.subscribe();

				async function newGame( roomId: ObjectId, ruleSet: RuleSet, selectedColors: ReadonlyArray<string>, size: Size ) {
					const rules = ruleSetMap.get( ruleSet );
					const gameId = new ObjectId;
					const gameState = c2sGameState( rules.getInitialState( size ) );
					if( selectedColors.length !== rules.seats ) throw new Error( 'Incorrect number of seats' );
					if( selectedColors.length !== [ ...new Set( selectedColors ).values() ].length ) throw new Error( 'Duplicate color' );

					const colorNames = Object.keys( colors );
					if( !selectedColors.every( color => colorNames.includes( color ) ) ) throw new Error( 'Unknown color' );
					if( !rules.availableWidths.includes( size.width ) || !rules.availableHeights.includes( size.height ) ) throw new Error( 'Invalid size' );

					const game: ServerGame = {
						_id: gameId,
						colors: [ ...selectedColors ],
						gameStates: [ gameState ],
						ruleSet
					};
					await collections.games.insertOne( s2mGame( game ) );
					const oldGameId = ( await collections.rooms.findOneAndUpdate( { _id: { $eq: roomId } }, { $set: { gameId } }, { projection: { gameId: true } } ) ).gameId;
					let oldRuleSet: RuleSet;
					if( oldGameId != null ) {
						oldRuleSet = ( await collections.games.findOne( { _id: { $eq: oldGameId } }, { projection: { ruleSet: true } } ) )?.ruleSet;
					}
					localBus.next( { type: BusMessageType.UpdateRoom, data: {} } );
					const roomIdStr = roomId.toHexString();
					const ruleSetName = ruleSetMap.get( ruleSet ).name;
					io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message: `New game - ${ruleSetName} rules` } } );
					if( oldRuleSet != null && oldRuleSet !== ruleSet ) {
						io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message: `Rules have changed to ${ruleSetName}` } } );
					}
					io.to( roomIdStr ).emit( 'message', { type: 'update', data: s2cGame( game ) } );
					return game;
				}

				handleMessage( socket, 'createRoom', async ( { name, password } ) => {
					if( !isValidRoomName( name ) ) throw new Error( 'Invalid room name.' );
					const roomId = new ObjectId;
					const roomIdStr = roomId.toHexString();
					await collections.rooms.insertOne( {
						_id: roomId,
						name,
						gameId: null,
						passwordHash: await hashPassword( password )
					} as ServerRoom );
					socket.join( roomIdStr );
					await collections.roomSessions.updateOne(
						{ roomId, sessionId },
						{ $setOnInsert: { roomId, sessionId, seats: [] } },
						{ upsert: true }
					);
					localBus.next( { type: BusMessageType.UpdateRoom, data: {} } );
					localBus.next( { type: BusMessageType.UpdateRoomSession, data: {} } );
					return roomIdStr;
				} );

				handleMessage( socket, 'newGame', async ( { roomId: roomIdStr, ruleSet, colors, size } ) => {
					const roomId = new ObjectId( roomIdStr );
					return s2cGame( await newGame( roomId, ruleSet, colors, size ) );
				} );

				handleMessage( socket, 'joinRoom', async ( { roomId: roomIdStr, password } ) => {
					const roomId = new ObjectId( roomIdStr );
					const nick = await getNick( sessionId );
					const room = await collections.rooms.findOne( { _id: roomId } );
					if( !room ) throw new Error( 'No such room.' );
					if( room.passwordHash ) {
						if( !password ) throw new Error( 'Room requires a password.' );
						if( !await checkPassword( password, room.passwordHash ) ) throw new Error( 'Incorrect password.' );
					}
					socket.join( roomIdStr );
					await collections.roomSessions.updateOne( { roomId, sessionId }, { $setOnInsert: {
						roomId,
						sessionId,
						seats: []
					} }, { upsert: true } );
					await collections.expirations.deleteOne( { _id: roomId } );
					if( room.gameId ) {
						const game = await collections.games.findOne( { _id: room.gameId } );
						if( game ) {
							await io.to( sessionIdStr ).emit( 'message', { type: 'update', data: m2cGame( game as MongoGame ) } );
						}
					}
					localBus.next( { type: BusMessageType.UpdateSession, data: {} } );
					localBus.next( { type: BusMessageType.UpdateRoomSession, data: {} } );
					const message = `${nick} has joined the room.`;
					io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message } } );
					return room._id.toHexString();
				} );

				handleMessage( socket, 'leaveRoom', async ( { roomId: roomIdStr } ) => {
					const roomId = new ObjectId( roomIdStr );
					const nick = await getNick( sessionId );
					socket.leave( roomIdStr );
					const { deletedCount } = await collections.roomSessions.deleteMany( { sessionId, roomId } );
					if( deletedCount > 0 ) {
						const message = `${nick} has left the room.`;
						io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message } } );
					}
					localBus.next( { type: BusMessageType.UpdateSession, data: {} } );
					localBus.next( { type: BusMessageType.UpdateRoomSession, data: {} } );
				} );

				handleMessage( socket, 'sendMessage', async ( { roomId: roomIdStr, message } ) => {
					const roomId = new ObjectId( roomIdStr );
					const nick = await getNick( sessionId );
					if( !await collections.roomSessions.findOne( { roomId, sessionId }, { projection: { _id: 1 } } ) ) {
						throw new Error( 'Not in room.' );
					}
					io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, user: nick, message } } );
				} );

				handleMessage( socket, 'sit', async ( { roomId: roomIdStr, seat } ) => {
					const roomId = new ObjectId( roomIdStr );
					const existingSeats = await collections.roomSessions.find( { roomId: { $eq: roomId }, sessionId: { $ne: sessionId }, seats: { $elemMatch: { $eq: new Int32( seat ) } } } ).toArray();
					if( existingSeats.length > 0 ) throw new Error( 'Seat not available.' );
					const { modifiedCount } = await collections.roomSessions.updateOne( { roomId, sessionId, seat: { $not: { $elemMatch: { $eq: seat } } } }, { $addToSet: { seats: new Int32( seat ) } } );
					if( modifiedCount === 0 ) return;
					localBus.next( { type: BusMessageType.UpdateRoomSession, data: {} } );
					const { gameId } = ( await collections.rooms.findOne( { _id: roomId, gameId: { $ne: null } }, { projection: { _id: 0, gameId: 1 } } ) ) || { gameId: null };
					if( !gameId ) return;
					const game = await collections.games.findOne( { _id: gameId } );
					if( !game ) return;
					const color = colors[ game.colors[ seat ] ];
					if( !color ) return;
					const nick = await getNick( sessionId );
					const message = `${nick} is now playing as ${color.displayName}.`;
					io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message } } );
				} );

				handleMessage( socket, 'stand', async ( { roomId: roomIdStr, seat } ) => {
					const roomId = new ObjectId( roomIdStr );
					const { modifiedCount } = await collections.roomSessions.updateOne( { roomId, sessionId, seats: { $elemMatch: { $eq: seat } } }, { $pull: { seats: new Int32( seat ) as any } } );
					if( modifiedCount === 0 ) return;
					localBus.next( { type: BusMessageType.UpdateRoomSession, data: {} } );
					const { gameId } = ( await collections.rooms.findOne( { _id: roomId, gameId: { $ne: null } }, { projection: { _id: 0, gameId: 1 } } ) ) || { gameId: null };
					if( !gameId ) return;
					const game = await collections.games.findOne( { _id: gameId } );
					if( !game ) return;
					const color = colors[ game.colors[ seat ] ];
					if( !color ) return;
					const nick = await getNick( sessionId );

					const message = `${nick} has stopped playing as ${color.displayName}.`;
					io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message } } );
				} );

				handleMessage( socket, 'setNick', async ( { nick } ) => {
					const oldNick = await getNick( sessionId );
					await collections.sessions.updateOne( { _id: sessionId }, { $set: { nick } } );
					const roomIds = await collections.roomSessions.find( { sessionId } ).project( { roomId: 1 } ).map( r => r.roomId ).toArray() as readonly ObjectId[];
					localBus.next( { type: BusMessageType.UpdateSession, data: {} } );
					if( roomIds.length ) {
						const message = `${oldNick} is now known as ${nick}.`;
						for( const roomId of roomIds ) {
							const roomIdStr = roomId.toHexString();
							io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message } } );
						}
					}
				} );

				handleMessage( socket, 'nickAvailable', async ( { nick } ) => {
					const count = await collections.users.countDocuments( { nick } );
					return count === 0;
				} );

				handleMessage( socket, 'newUser', async ( { nick, password } ) => {
					if( !isValidNick( nick ) ) throw new Error( 'Invalid nick.' );
					if( !isValidUserPassword( password ) ) throw new Error( 'Invalid password.' );
					const userId = new ObjectId;
					await collections.sessions.updateMany( {
						nick,
						userId: { $ne: userId }
					}, {
						$set: { nick: 'Guest' }
					} );
					localBus.next( { type: BusMessageType.UpdateSession, data: {} } );
					return userId.toHexString();
				} );

				handleMessage( socket, 'logIn', async ( { nick, password } ) => {
					const user = await collections.users.findOne( { nick } );
					if( !checkPassword( password, user?.passwordHash ) || user == null ) throw new Error( 'Invalid nick or password.' );
					await collections.sessions.updateOne( { _id: sessionId }, { $set: {
						userId: user._id,
						nick: user.nick
					} } );
					localBus.next( { type: BusMessageType.UpdateSession, data: {} } );
					return user._id.toHexString();
				} );

				handleMessage( socket, 'logOut', async () => {
					await collections.sessions.deleteOne( { _id: sessionId } );
					await collections.roomSessions.deleteMany( { sessionId } );
					localBus.next( { type: BusMessageType.UpdateSession, data: {} } );
					localBus.next( { type: BusMessageType.UpdateRoomSession, data: {} } );
					socket.disconnect();
				} );

				handleMessage( socket, 'makeMove', async ( { roomId: roomIdStr, position } ) => {
					const roomId = new ObjectId( roomIdStr );
					const roomSession = await collections.roomSessions.findOne( { roomId, sessionId } );
					if( !roomSession ) {
						throw new Error( 'Not in room.' );
					}
					const room = await collections.rooms.findOne( { _id: roomId } );
					if( !room ) throw new Error( 'Room not found.' );
					const gameId = room.gameId;
					const serverGame = m2sGame( await collections.games.findOne( { _id: gameId } ) as MongoGame );
					if( !serverGame ) {
						throw new Error( 'Game not found.' );
					}
					const game = s2cGame( serverGame );
					const rules = ruleSetMap.get( game.ruleSet );
					const gameStates = [ ...game.gameStates ];
					const oldGameState = gameStates.slice( -1 )[ 0 ];
					const seat = oldGameState.turn;
					if( !rules.isValid( oldGameState, position, seat ) ) {
						throw new Error( 'Invalid move.' );
					}
					if( !roomSession.seats.map( v => v.valueOf() ).includes( seat ) ) {
						let fail = true;
						if( ( await collections.roomSessions.countDocuments( { roomId: { $eq: roomId }, sessionId: { $ne: sessionId }, seats: { $elemMatch: { $eq: new Int32( seat ) } } } ) ) === 0 ) {
							const { modifiedCount } = await collections.roomSessions.updateOne( { roomId, sessionId, seats: { $not: { $elemMatch: { $eq: new Int32( seat ) } } } }, { $addToSet: { seats: new Int32( seat ) } } );
							if( modifiedCount > 0 ) fail = false;
						}
						if( fail ) {
							throw new Error( 'Wrong turn.' );
						}
						localBus.next( { type: BusMessageType.UpdateRoomSession, data: {} } );
						const colorData = colors[ game.colors[ seat ] ];
						const nick = await getNick( sessionId );
						const message = `${nick} is now playing as ${colorData.displayName}.`;
						io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message } } );
					}
					const newGameState = rules.makeMove( oldGameState, position );
					if( !newGameState ) {
						throw new Error( 'Invalid move.' );
					}
					await collections.games.updateOne( { _id: gameId }, {
						$push: { gameStates: c2mGameState( newGameState ) }
					} );
					game.gameStates = [ ...gameStates, newGameState ];
					io.to( roomIdStr ).emit( 'message', { type: 'update', data: game } );
					if( rules.isGameOver( newGameState ) ) {
						const scores =
						Array.from( { length: rules.seats } )
						.map( ( _, seat ) => ( {
							color: colors[ game.colors[ seat ] ].displayName,
							score: rules.getScore( newGameState, seat )
						} ) );
						scores.sort( ( c1, c2 ) => {
							const r1 = rules.compareScores( c1.score, c2.score );
							return ( r1 === 0 ) ? c1.color.localeCompare( c2.color ) : r1;
						} );
						const bestScore = scores[ 0 ].score;
						const winners = scores.filter( ( { score } ) => rules.compareScores( score, bestScore ) === 0 );
						let message: string;
						if( winners.length === 1 ) {
							message = `${winners[ 0 ].color} wins`;
						} else {
							message = 'Draw game';
						}
						io.to( roomIdStr ).emit( 'message', { type: 'message', data: { roomId: roomIdStr, message: `${message}:\n\n| Color | Score |\n| - | - |\n${scores.map( ( { color, score } ) => `| ${color} | ${score} |` ).join( '\n' )}\n` } } );
					}
				} );

				disconnected.subscribe( async () => {
					const nick = await getNick( sessionId );
					const message = `${nick} has disconnected.`;
					const roomIds = await collections.roomSessions.find( { query: { sessionId } } ).map( r => r.roomId.toHexString() as unknown as Document ).toArray() as unknown as readonly string[];
					for( const roomId of roomIds ) {
						io.to( roomId ).emit( 'message', { type: 'message', data: { roomId, message } } );
					}
				} );

				disconnected.subscribe( async () => {
					log.info( `User disconnected, ${--connections} connected` );
				} );
			} catch( ex ) {
				log.fatal( ex );
				throw ex;
			}
		} );
	} catch( ex ) {
		log.fatal( ex );
	}
} )();
