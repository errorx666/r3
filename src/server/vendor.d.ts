declare module 'bcryptjs' {
	export function genSalt( rounds?: number ): Promise<string>;
	export function hash( data: string, salt: string, progress: Function|null ): Promise<string>;
	export function compare( data: string, encrypted: string ): Promise<boolean>;
}

declare module 'couchdb-adapter-leveldb';
