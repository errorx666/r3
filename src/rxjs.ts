import { pipe } from 'rxjs';
import { scan, switchAll } from 'rxjs/operators';

export const asyncScan =
	<T, U>( fn: ( prev: U, next: T ) => U|PromiseLike<U>, initialValue?: U|PromiseLike<U> ) =>
	pipe(
		scan<T, Promise<U>>( async ( prev, next ) => Promise.resolve( fn( await prev, next ) ), Promise.resolve( initialValue ) ),
		switchAll()
	);
