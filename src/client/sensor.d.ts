declare interface Sensor extends EventTarget {
	readonly activated: boolean;
	readonly hasReading: boolean;
	readonly timestamp: number;
	start();
	stop();
	addEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorrEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
	addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
	removeEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
	removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
}

declare interface SensorErrorEvent extends Event {
	error: DOMException;
}

interface SensorEventMap {
	'error': SensorErrorEvent;
	'reading': Event;
	'activate': Event;
}

declare type SensorOptions = {
	frequency?: number;
	referenceFrame?: 'device'|'screen';
};

declare class Accelerometer implements Sensor {
	public constructor( options?: SensorOptions );
	public readonly x: number;
	public readonly y: number;
	public readonly z: number;
	public readonly activated: boolean;
	public readonly hasReading: boolean;
	public readonly timestamp: number;
	public start();
	public stop();
	public addEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorrEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
	public addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
	public removeEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
	public removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
	public dispatchEvent(event: Event): boolean;
}

declare class Gyroscope implements Sensor {
	public constructor( options?: SensorOptions );
	public readonly x: number;
	public readonly y: number;
	public readonly z: number;
	public readonly activated: boolean;
	public readonly hasReading: boolean;
	public readonly timestamp: number;
	public start();
	public stop();
	public addEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorrEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
	public addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
	public removeEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
	public removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
	public dispatchEvent(event: Event): boolean;
}

declare class Magnetometer implements Sensor {
	public constructor( options?: SensorOptions );
	public readonly x: number;
	public readonly y: number;
	public readonly z: number;
	public readonly activated: boolean;
	public readonly hasReading: boolean;
	public readonly timestamp: number;
	public start();
	public stop();
	public addEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorrEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
	public addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
	public removeEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
	public removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
	public dispatchEvent(event: Event): boolean;
}

declare interface OrientationSensor extends Sensor {
	quaternion: [ number, number, number, number ];
	populateMatrix( targetMatrix: Float32Array|Float64Array|DOMMatrix );
}

declare class AbsoluteOrientationSensor implements OrientationSensor {
	public constructor( options?: SensorOptions );
	public readonly activated: boolean;
	public readonly hasReading: boolean;
	public readonly timestamp: number;
	public start();
	public stop();
	public quaternion: [ number, number, number, number ];
	public populateMatrix( targetMatrix: Float32Array|Float64Array|DOMMatrix );
	public addEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorrEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
	public addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
	public removeEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
	public removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
	public dispatchEvent(event: Event): boolean;
}

declare class RelativeOrientationSensor implements OrientationSensor {
	public constructor( options?: SensorOptions );
	public readonly activated: boolean;
	public readonly hasReading: boolean;
	public readonly timestamp: number;
	public start();
	public stop();
	public quaternion: [ number, number, number, number ];
	public populateMatrix( targetMatrix: Float32Array|Float64Array|DOMMatrix );
	public addEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorrEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
	public addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
	public removeEventListener<K extends keyof SensorEventMap>(type: K, listener: (this: this, ev: SensorEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
	public removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
	public dispatchEvent(event: Event): boolean;
}
