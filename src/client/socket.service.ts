import { Observable, ReplaySubject, Subject, fromEvent, of } from 'rxjs';
import { map, takeUntil, filter, mergeMap, take } from 'rxjs/operators';
import { Injectable, OnDestroy } from '@angular/core';
import { io } from 'socket.io-client';
import type { Socket } from 'socket.io-client';

@Injectable()
export class SocketService implements OnDestroy {
	constructor() {
		const { destroyed } = this;

		const isProduction = process.env.NODE_ENV === 'production';

		const socket = this.socket = io( {
			autoConnect: false,
			reconnection: true,
			transports: [ 'websocket', 'polling' ],
			upgrade: true,
			rejectUnauthorized: isProduction,
			get query() {
				let sessionId: string;
				try {
					sessionId = JSON.parse( sessionStorage.getItem( 'sessionId' ) );
				} catch { /* do nothing */ }
				return {
					...( sessionId ? { sessionId } : {} )
				};
			}
		} );

		fromEvent( socket, 'message' )
		.pipe( takeUntil( destroyed ) )
		.subscribe( this.messages );

		fromEvent( socket, 'reconnect_attempt' )
		.pipe( takeUntil( destroyed ) )
		.subscribe( () => { console.log( 'Attempting to reconnect...' ); } );

		fromEvent( socket, 'connect_error' )
		.pipe(
			take( 1 ),
			takeUntil( destroyed )
		)
		.subscribe( () => {
			socket.io.opts.transports = [ 'polling', 'websocket' ];
		} );

		of( 'error', 'connect_error', 'reconnect_error', 'reconnect_failed' )
		.pipe(
			mergeMap( e => fromEvent( socket, e ) ),
			takeUntil( destroyed )
		)
		.subscribe( err => {
			console.error( err );
		} );

		socket.connect();

		destroyed.subscribe( () => { socket.close(); } );
	}

	public getMessages<T>( type: string ): Observable<T> {
		const { messages } = this;
		return messages
		.pipe(
			filter( m => m.type === type ),
			map( m => m.data )
		);
	}

	public send<T>( type: string, data: object = {} ) {
		const { socket } = this;
		return new Promise<T>( ( resolve, reject ) => {
			socket.send( { type, data }, ( error, result ) => {
				if( error ) reject( error );
				else resolve( result );
			} );
		} );
	}

	public ngOnDestroy() {
		this.destroyed.next( true );
		this.destroyed.complete();
	}

	private readonly messages = new ReplaySubject<{ type: string, data: any }>( 20 );
	private readonly socket: Socket;
	private readonly destroyed = new Subject<true>();
}
