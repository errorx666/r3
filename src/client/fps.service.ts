/// <reference path="./stats-js.d.ts"/>

import { Injectable } from '@angular/core';
import { showFps } from '~data/app.config.yaml';
import Stats from 'stats-js';
let stats: Stats|null = null;

const loaded = showFps
?	import( 'stats-js' )
	.then( ( { default: Stats } ) => stats = new Stats )
:	Promise.resolve<Stats>( null );

@Injectable()
export class FpsService {
	public beforeFrame() {
		if( stats ) stats.begin();
	}

	public afterFrame() {
		if( stats ) stats.end();
	}

	get enabled() { return showFps; }

	get domElement() { return loaded.then( s => s && s.dom ); }
}
