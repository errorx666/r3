import { Injectable } from '@angular/core';
import { Observable, merge, fromEvent, throwError, timer } from 'rxjs';
import { shareReplay, map, switchMap, filter } from 'rxjs/operators';
import sensorOptions from '~data/sensor.config.yaml';

function observeSensor<TSensor extends Sensor, TValue>( sensor: TSensor, get: ( sensor: TSensor ) => TValue ) {
	return new Observable<TValue>( observer => {
		merge(
			timer( 0 ),
			fromEvent( sensor, 'reading', { capture: false, passive: true } ),
			fromEvent( sensor, 'active', { capture: false, passive: true } ),
			fromEvent( sensor, 'error', { capture: false, passive: true } )
			.pipe( switchMap<SensorErrorEvent, Observable<never>>( e => throwError( () => e.error ) ) )
		)
		.pipe( filter( () => sensor.hasReading ), map( () => get( sensor ) ) )
		.subscribe( observer );

		sensor.start();
		return () => { sensor.stop(); };
	} )
	.pipe( shareReplay( 1 ) );
}

@Injectable()
export class SensorService {
	constructor() {
		const degToRad = ( x: number ) => x * Math.PI / 180;
		try {
			this.absoluteOrientationObservable = observeSensor( new AbsoluteOrientationSensor( sensorOptions.absoluteOrientation ), s => s.quaternion );
		} catch( ex ) {
			console.error( ex );
			this.absoluteOrientationObservable = throwError( () => new Error( 'AbsoluteOrientationSensor unsupported' ) );
		}
		try {
			this.gyroscopeObservable = observeSensor( new Gyroscope( sensorOptions.gyroscope ), ( { x, y, z } ) => [ x, y, z ] as [ number, number, number ] );
		} catch( ex ) {
			console.error( ex );
			this.gyroscopeObservable = throwError( () => new Error( 'Gyroscope unsupported' ) );
		}
		this.deviceOrientationObservable = new Observable<[number, number, number]>( observer => {
			fromEvent<DeviceOrientationEvent>( window, 'deviceorientation', { capture: false, passive: true } )
			.pipe( map( e => [ e.beta, e.gamma, e.alpha ].map( degToRad ) ) )
			.subscribe( observer );
			return () => {};
		} )
		.pipe( shareReplay( 1 ) );
	}

	public getAbsoluteOrientation() {
		return this.absoluteOrientationObservable;
	}

	public getGyroscope() {
		return this.gyroscopeObservable;
	}

	public getDeviceOrientation() {
		return this.deviceOrientationObservable;
	}

	private readonly gyroscopeObservable: Observable<[number, number, number]>;
	private readonly absoluteOrientationObservable: Observable<[number, number, number, number]>;
	private readonly deviceOrientationObservable: Observable<[number, number, number]>;
}
