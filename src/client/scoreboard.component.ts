import { ruleSetMap } from '~rule-sets';

import { combineLatest, Subject, of, Observable } from 'rxjs';

import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

import { GameService } from './game.service';
import { RoomService } from './room.service';
import { map, takeUntil, switchMap } from 'rxjs/operators';
import { ModalNewGameComponent } from './modal/new-game.component';
import { colors } from '~data/colors.yaml';
import { SessionService } from './session.service';

interface Score {
	color: string;
	seat: number;
	score: number;
	player: string;
	me: boolean;
	hasTurn: boolean;
}

@Component( {
	selector: 'scoreboard',
	templateUrl: './scoreboard.component.html',
	styleUrls: [ './scoreboard.component.css' ],
	changeDetection: ChangeDetectionStrategy.OnPush
} )
export class ScoreboardComponent implements OnInit, OnDestroy {
	constructor(
		private readonly changeDetector: ChangeDetectorRef,
		private readonly sessionService: SessionService,
		private readonly gameService: GameService,
		private readonly roomService: RoomService
	) {}

	@ViewChild( 'newGameModal', { static: false } )
	public newGameModal: ModalNewGameComponent;

	public ngOnInit() {
		const { gameService, roomService, sessionService, destroyed } = this;

		const sessionId = sessionService.getSessionId();
		const allSessions = sessionService.getSessionMap();
		const currentRoom = roomService.getCurrentRoom();

		const roomSessions =
			currentRoom
			.pipe(
				switchMap<ClientRoom, Observable<ReadonlyArray<ClientRoomSession>>>( room => room ? roomService.getRoomSessions( room.id ) : of( [] ) )
			);

		const sessions =
			roomSessions
			.pipe(
				switchMap( rs =>
					allSessions
					.pipe(
						map( ss => rs.map( rs => ( {
							...ss.get( rs.sessionId ),
							seats: [ ...rs.seats ]
						} ) ) )
					)
				)
			);

		const currentGame =
			combineLatest( [
				gameService.getGames(),
				currentRoom
			] )
			.pipe(
				map( ( [ games, room ] ) =>
					room ? games.get( room.gameId ) : null
				),
				takeUntil( destroyed )
			);

		combineLatest( [ sessionId, currentGame, sessions ] )
		.pipe(
			map( ( [ sessionId, game, sessions ] ) => {
				if( !game ) return [];
				const rules = ruleSetMap.get( game.ruleSet );
				const gameState = game.gameStates.slice( -1 )[ 0 ];
				return game.colors.map( ( colorKey, seat ) => {
					const color = colors[ colorKey ].displayName;
					const score = rules.getScore( gameState, seat );
					let player: string = null;
					let me = false;
					for( const session of sessions ) {
						if( session.seats.includes( seat ) ) {
							player = session.nick;
							me = session.id === sessionId;
						}
					}
					const hasTurn = seat === gameState.turn;
					return { color, seat, score, player, me, hasTurn };
				} );
			} ),
			map( s => s.filter( Boolean ) ),
			takeUntil( destroyed )
		).subscribe( scores => {
			this.scores = scores;
			this.changeDetector.markForCheck();
		} );

		currentRoom
		.pipe( takeUntil( destroyed ) )
		.subscribe( room => {
			this.roomId = room ? room.id : null;
			this.changeDetector.markForCheck();
		} );

		currentGame
		.subscribe( game => {
			this.game = game;
			if( game ) {
				const gameState = this.gameState = game.gameStates.slice( -1 )[ 0 ];
				const c = this.colors = [ ...game.colors ];
				if( gameState.turn == null ) this.turn = null;
				else this.turn = colors[ c[ gameState.turn ] ].displayName;
				this.rules = ruleSetMap.get( game.ruleSet );
			} else {
				this.gameState = null;
				this.turn = null;
				this.rules = null;
			}
			this.changeDetector.markForCheck();
		} );
	}

	public ngOnDestroy() {
		this.destroyed.next( true );
		this.destroyed.complete();
	}

	public async sit( seat: number ) {
		const { roomId, roomService } = this;
		await roomService.sit( roomId, seat );
	}

	public async stand( seat: number ) {
		const { roomId, roomService } = this;
		await roomService.stand( roomId, seat );
	}

	public colors = null as string[];
	public turn = null as string|null;
	public roomId = null as string|null;
	public game = null as ClientGame|null;
	public gameState = null as ClientGameState|null;
	public lastMove = null as Point|null;
	public rules = null as Rules|null;

	public scores = [] as Score[];

	private readonly destroyed = new Subject<true>();
}
