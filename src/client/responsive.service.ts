import { Injectable, OnDestroy } from '@angular/core';

import isEqual from 'lodash/isEqual';
import { Subject, Observable, merge, fromEvent, interval } from 'rxjs';
import { distinctUntilChanged, map, shareReplay, takeUntil, startWith } from 'rxjs/operators';
import { breakpoints } from '~data/responsive.yaml';

function compareSize( size1: Size, size2: Size ) {
	if( !size1 || !size2 ) return size1 === size2;
	return size1.width === size2.width && size1.height === size2.height;
}

@Injectable()
export class ResponsiveService implements OnDestroy {
	readonly #scroll: Observable<Event>;
	readonly #size: Observable<Size>;
	readonly #destroyed = new Subject<true>();

	public constructor() {
		this.#scroll =
			fromEvent( window, 'scroll', { passive: true } )
			.pipe( takeUntil( this.#destroyed ) );
		this.#size =
			fromEvent( window, 'resize', { passive: true } )
			.pipe(
				startWith( null ),
				map( () => ( { width: window.innerWidth, height: window.innerHeight } ) ),
				distinctUntilChanged( compareSize ),
				shareReplay( 1 ),
				takeUntil( this.#destroyed )
			);
	}

	public getBreakpoint() {
		return this.#size.pipe(
			map( ( { width } ) => {
				for( const [ key, max ] of Object.entries( breakpoints ) ) {
					if( width < max ) return key as ResponsiveBreakpoint;
				}
			} ),
			distinctUntilChanged()
		);
	}

	public getResize( e: HTMLElement ) {
		return new Observable<readonly ResizeObserverEntry[]>( observer => {
			const ro = new ResizeObserver( r => {
				observer.next( r );
			} );
			ro.observe( e, {} );
			return () => {
				ro.disconnect();
			};
		} ).pipe(
			takeUntil( this.#destroyed )
		);
	}

	public getMutation( e: HTMLElement, options?: MutationObserverInit ) {
		return new Observable<readonly MutationRecord[]>( observer => {
			const mo = new MutationObserver( m => {
				observer.next( m );
			} );
			mo.observe( e, options );
			return () => {
				mo.disconnect();
			};
		} ).pipe(
			takeUntil( this.#destroyed )
		);
	}

	public getElementBoundingSize( e: HTMLElement ) {
		return this.getElementRect( e )
		.pipe(
			map( ( { width, height } ) => ( { width, height } ) ),
			distinctUntilChanged( compareSize ),
			takeUntil( this.#destroyed )
		);
	}

	public getElementClientSize( e: HTMLElement ) {
		return merge( [ this.getResize( e ), interval( 1000 ) ] )
		.pipe(
			startWith( null ),
			map( () => ( { width: e.clientWidth, height: e.clientHeight } ) ),
			distinctUntilChanged( compareSize ),
			shareReplay( 1 ),
			takeUntil( this.#destroyed )
		);
	}

	public getElementRect( e: HTMLElement ): Observable<DOMRectReadOnly> {
		return merge( [ this.#scroll, this.getResize( e ), this.getMutation( e, {
			attributes: true,
			childList: true,
			subtree: true
		} ), interval( 1000 ) ] )
		.pipe(
			startWith( null ),
			map( () => {
				const { width, top, left, right, bottom, height } = e.getBoundingClientRect();
				return { width, top, left, right, bottom, height };
			} ),
			distinctUntilChanged( isEqual ),
			shareReplay( 1 ),
			takeUntil( this.#destroyed )
		);
	}

	public getIntersection( e: HTMLElement, options?: IntersectionObserverInit ) {
		return new Observable<readonly IntersectionObserverEntry[]>( observer => {
			const io = new IntersectionObserver( i => {
				observer.next( i );
			}, options );
			io.observe( e );
			return () => {
				io.disconnect();
			};
		} );
	}

	public ngOnDestroy() {
		this.#destroyed.next( true );
		this.#destroyed.complete();
	}
}
