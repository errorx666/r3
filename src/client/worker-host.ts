import { Observable, fromEvent, merge, throwError, of, EMPTY } from 'rxjs';
import { share, switchMap, mergeMap, map } from 'rxjs/operators';
import { MessageMap } from './worker-messages';

interface WorkerHostOptions {
	readonly src: string;
}
export class WorkerHost<TMessageMap extends MessageMap = {}> {
	private readonly worker: Worker;
	private readonly error$: Observable<never>;
	private readonly message$: Observable<MessageEvent>;

	public constructor( options: WorkerHostOptions ) {
		const worker = this.worker = new Worker( options.src );
		const error$ = this.error$ =
			fromEvent<ErrorEvent>( worker, 'error' )
			.pipe( switchMap( e => throwError( () => e ) ) );

		error$.subscribe( {
			error: err => {
				console.error( err );
				worker.terminate();
			}
		} );

		this.message$ =
			merge(
				fromEvent<MessageEvent>( worker, 'message' ),
				error$
			).pipe(
				share()
			);
	}

	public sendRequest<
		TMessageKey extends keyof TMessageMap,
		TRequestKey extends keyof TMessageMap[TMessageKey] & 'request',
		TInKey extends keyof TMessageMap[TMessageKey] & 'in',
		TOutKey extends keyof TMessageMap[TMessageKey] & 'out'
	>(
		request: ( TMessageMap[TMessageKey][TRequestKey] extends never ? {} : TMessageMap[TMessageKey][TRequestKey] ) & { readonly type: TMessageKey },
		transfer = [] as readonly Transferable[],
		messagesIn = EMPTY as Observable<TMessageMap[TMessageKey][TInKey]>
	) {
		return new Observable<TMessageMap[TMessageKey][TOutKey]>( observer => {
			const { port1: portIn, port2: portOut } = new MessageChannel;
			merge(
				fromEvent<MessageEvent>( portOut, 'message' ),
				of( portIn, portOut )
				.pipe(
					mergeMap( port => fromEvent( port, 'messageerror' ) ),
					switchMap( e => throwError( () => e ) )
				),
				this.error$
			)
			.pipe( map( e => e.data ) )
			.subscribe( observer );

			this.worker.postMessage( { ...request, port: portIn }, [ ...transfer, portIn ] );
			portOut.start();

			messagesIn.subscribe( e => portIn.postMessage( e ) );

			return () => {
				portIn.close();
				portOut.close();
			};
		} )
		.pipe( share() );
	}
}
