import { Subject, combineLatest, Observable } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { Injectable, OnDestroy } from '@angular/core';
import { toArrayMap, toMap } from '~operators';
import { SocketService } from './socket.service';
import { SessionSubject } from './session-subject';

@Injectable()
export class SessionService implements OnDestroy {
	constructor(
		private readonly socketService: SocketService
	) {
		const { destroyed } = this;

		if( process.env.NODE_ENV === 'production' ) {
			socketService.getMessages<string>( 'build' )
			.pipe(
				filter( s => s !== BUILD_ID ),
				takeUntil( destroyed )
			)
			.subscribe( () => {
				( location as any ).reload( true );
			} );
		}

		socketService.getMessages<ClientRoomSession[]>( 'roomSessions' )
		.pipe(
			map( s => s || [] ),
			takeUntil( destroyed )
		)
		.subscribe( this.roomSessions );

		socketService.getMessages<ClientSession[]>( 'sessions' )
		.pipe(
			map( s => s || [] ),
			takeUntil( destroyed )
		)
		.subscribe( this.sessions );

		socketService.getMessages<{ sessionId: string }>( 'session' )
		.pipe(
			map( ( { sessionId } ) => sessionId ),
			takeUntil( destroyed )
		)
		.subscribe( this.sessionId );
	}

	public getSessionMap() {
		return this.sessions.pipe( toMap( e => e.id ) );
	}

	public getCurrentSession() {
		return combineLatest( [ this.getSessionMap(), this.getSessionId() ] )
		.pipe(
			map( ( [ sessions, sessionId ] ) =>
				sessions.get( sessionId ) || null
			)
		);
	}

	public getRoomSessionMqp() {
		const { roomSessions } = this;
		return roomSessions.pipe( toArrayMap( e => e.roomId ) );
	}

	public getSessionRoomMap() {
		const { roomSessions } = this;
		return roomSessions.pipe( toArrayMap( e => e.sessionId ) );
	}

	public getCurrentRoomSessions() {
		return combineLatest( [ this.getSessionRoomMap(), this.sessionId ] )
		.pipe(
			map( ( [ sr, sessionId ] ) =>
				sr.get( sessionId ) || []
			)
		);
	}

	public async newUser( nick: string, password: string ) {
		const { socketService } = this;
		return await socketService.send<string>( 'newUser', { nick, password } );
	}

	public async logIn( nick: string, password: string ) {
		const { socketService } = this;
		return await socketService.send<string>( 'logIn', { nick, password } );
	}

	public async logOut() {
		const { socketService } = this;
		await socketService.send( 'logOut' );
	}

	public getSessionId() {
		const { sessionId } = this;
		return sessionId as Observable<string>;
	}

	public ngOnDestroy() {
		this.destroyed.next( true );
		this.destroyed.complete();
	}

	private readonly sessionId = new SessionSubject<string>( 'sessionId', null );
	private readonly sessions = new SessionSubject<ClientSession[]>( 'sessions', [] );
	private readonly roomSessions = new SessionSubject<ClientRoomSession[]>( 'roomSessions', [] );
	private readonly destroyed = new Subject<true>();
}
