/* eslint key-spacing: "off", no-multi-spaces: "off" */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ModalChooseComponent } from './choose.component';
import { ModalCreateRoomComponent } from './create-room.component';
import { ModalHeaderComponent } from './header.component';
import { ModalNewGameComponent } from './new-game.component';
import { ModalJoinRoomComponent } from './join-room.component';
import { ModalSignInComponent } from './sign-in.component';
import { ComponentLoaderFactory } from 'ngx-bootstrap/component-loader';
import { ModalModule as BsModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownConfig, BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ColorPipe } from '~client/color.pipe';
import { ValidatorModule } from '~client/validator/validator.module';

@NgModule( {
	imports:      [ CommonModule, FormsModule, BsModalModule.forRoot(), BsDropdownModule.forRoot(), FontAwesomeModule, ValidatorModule ],
	exports:      [ ModalChooseComponent, ModalCreateRoomComponent, ModalNewGameComponent, ModalJoinRoomComponent, ModalSignInComponent ],
	declarations: [ ModalChooseComponent, ModalCreateRoomComponent, ModalHeaderComponent, ModalNewGameComponent, ModalJoinRoomComponent, ModalSignInComponent, ColorPipe ],
	providers:    [ ComponentLoaderFactory, { provide: BsDropdownConfig, useValue: { isAnimated: true } } ]
} )
export class ModalModule {}
