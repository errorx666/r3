import { Component, ViewChild, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef } from '@angular/core';
import { GameService } from '~client/game.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ruleSets, rulesStandard } from '~rule-sets';
import { ColorDefinition, colors } from '~data/colors.yaml';

@Component( {
	selector: 'modal-new-game',
	templateUrl: 'new-game.component.html',
	styleUrls: [ './new-game.component.css' ],
	changeDetection: ChangeDetectionStrategy.OnPush
} )
export class ModalNewGameComponent implements OnInit {
	constructor(
		public readonly hostElement: ElementRef,
		private readonly changeDetector: ChangeDetectorRef,
		private readonly gameService: GameService
	) {}

	public contrastColor( [ h, s, l ]: [ number, number, number ] ) {
		return [ h + 180 % 360, s, 100 - l ];
	}

	public getAvailableColors( index: number ) {
		const otherColors = this.selectedColors.filter( ( _, i ) => i !== index );
		return Object.values( colors )
		.filter(
			availableColor => !otherColors.includes( availableColor )
		);
	}

	public ngOnInit() {
		if( !this.rules ) this.selectRules( rulesStandard );
	}

	public selectRules( rules: Rules ) {
		this.rules = rules;
		this.selectedColors = rules.defaultColors.map( c => colors[ c ] );
		this.availableWidths = rules.availableWidths;
		this.availableHeights = rules.availableHeights;
		this.selectedWidth = rules.defaultSize.width;
		this.selectedHeight = rules.defaultSize.height;
	}

	@ViewChild( 'newGameModal', { static: false } )
	protected newGameModal: ModalDirective;

	public show( roomId: string ) {
		const { newGameModal, changeDetector } = this;
		this.roomId = roomId;
		newGameModal.show();
		changeDetector.markForCheck();
	}

	public hide() {
		const { newGameModal } = this;
		newGameModal.hide();
	}

	public roomId: string|null;
	public rules: Rules;
	public selectedColors: ColorDefinition[];
	public availableWidths: ReadonlyArray<number>;
	public availableHeights: ReadonlyArray<number>;
	public selectedWidth: number;
	public selectedHeight: number;

	public get selectedSize() {
		return { width: this.selectedWidth, height: this.selectedHeight };
	}
	public readonly availableRules = ruleSets;

	public async newGame() {
		const { gameService, roomId, rules, selectedColors, selectedSize } = this;
		this.hide();
		await gameService.newGame( roomId, rules.ruleSet, selectedColors.map( c => c.key ), selectedSize );
	}
}
