import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faComment } from '@fortawesome/free-regular-svg-icons';
import { faDoorClosed, faDoorOpen, faGlobe, faKey, faLock, faTimes, faTerminal, faUser, faSpinner } from '@fortawesome/free-solid-svg-icons';

export function registerFonts( library: FaIconLibrary ) {
	library.addIcons( faTwitter );
	library.addIcons( faComment );
	library.addIcons( faDoorClosed, faDoorOpen, faGlobe, faKey, faLock, faTimes, faTerminal, faUser, faSpinner );
}
