import { Board } from '~board';
import { AfterViewInit, Component, ViewChild, ElementRef, Input, Output, OnChanges, EventEmitter, OnDestroy, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ReplaySubject, Subject, animationFrameScheduler, combineLatest, fromEvent, interval, merge, of } from 'rxjs';
import { map, filter, switchMap, mergeMap, takeUntil, distinctUntilChanged, debounceTime, catchError, sample, scan, repeat, skip, take, shareReplay } from 'rxjs/operators';
import { colors } from '~data/colors.yaml';
import boardSettings from '~data/board.yaml';

import { Scene, SpotLight, Color, PCFSoftShadowMap, PCFShadowMap, BasicShadowMap, AmbientLight, Raycaster, Object3D, Vector3, DirectionalLight, PointLight, Mesh, OrthographicCamera, WebGLRenderer, AnimationMixer, Clock, LoopOnce, Quaternion, Euler, HemisphereLight, Light, DirectionalLightShadow, PointLightShadow, SpotLightShadow, Group, PerspectiveCamera, CameraHelper, DirectionalLightHelper, PointLightHelper, HemisphereLightHelper, SpotLightHelper, BoxHelper, GridHelper, AxesHelper, Vector2, DisplayP3ColorSpace, VSMShadowMap, ReinhardToneMapping, BufferGeometry, Object3DEventMap, MeshStandardMaterial } from 'three';
import { AnimationAction } from 'three/src/animation/AnimationAction';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { OutputPass } from 'three/examples/jsm/postprocessing/OutputPass';
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader';
import { Grid } from '~grid';
import { SensorService } from './sensor.service';
import { FpsService } from './fps.service';
import { ResponsiveService } from './responsive.service';
import { generateFadeClip, generateFlipClip, enableShadows, getPiece, setPieceTopColor, setPieceBottomColor, getMarker, getBorder, getBoard, getText, disposeMaterials, getDescendants, getArray, disposeOffscreenRenderer } from './builder';

const borderScale = .1;

type UserData<B extends { userData?: any;}, T> = Omit<B, 'userData'> & { userData: T };

type FadeUserData = {
	actions: {
		fadeIn: AnimationAction;
		fadeOut: AnimationAction;
	};
};

type SceneUserData = {
	lights: Group;
	fixedLights: Group;
	mixer: AnimationMixer;
	squareObjects: Grid<SquareObjects>;
};

type MarkerUserData = FadeUserData & {};

type PieceUserData = FadeUserData & {
	actions: {
		flip: AnimationAction;
	};
};

type BoardUserData = {
	position: Point;
};

type MeshMarker = UserData<Mesh<BufferGeometry, MeshStandardMaterial, Object3DEventMap>, MarkerUserData>;

type MeshBoard = UserData<Mesh<BufferGeometry, MeshStandardMaterial, Object3DEventMap>, BoardUserData>;

type MeshPiece = UserData<Mesh<BufferGeometry, MeshStandardMaterial[], Object3DEventMap>, PieceUserData>;

type SceneScene = UserData<Scene, SceneUserData>;

interface SquareObjects {
	board: MeshBoard;
	marker: MeshMarker;
	piece: MeshPiece;
}

function distance( pt1: Point, pt2: Point ) {
	return Math.max( Math.abs( pt1.x - pt2.x ), Math.abs( pt1.y - pt2.y ) );
}

function hslToColor( [ h, s, l ]: [ number, number, number ] ) {
	return ( new Color ).setHSL( h / 360, s * .01, l * .01 );
}

function cleanUserData( userData: any ) {
	if( typeof userData !== 'object' ) return;
	for( const key of Object.keys( userData ) ) {
		try {
			userData[ key ] = undefined;
			delete userData[ key ];
		} catch { /* do nothing */ }
	}
}

function disposeScene( scene: SceneScene ) {
	for( const squareObject of scene.userData.squareObjects ) {
		disposeMaterials( squareObject.marker );
		disposeMaterials( squareObject.piece );
		squareObject.board.geometry.dispose();
		cleanUserData( squareObject.board.userData );
		cleanUserData( squareObject.marker.userData );
		cleanUserData( squareObject.piece.userData );
	}
	scene.userData.squareObjects.clear();
	for( const o of getDescendants( scene ) ) {
		cleanUserData( o.userData );
	}
}

@Component( {
	selector: 'board',
	templateUrl: './board.component.html',
	styleUrls: [ './board.component.css' ],
	changeDetection: ChangeDetectionStrategy.OnPush
} )
export class BoardComponent implements AfterViewInit, OnChanges, OnDestroy, OnInit {
	public constructor(
		private readonly changeDetectorRef: ChangeDetectorRef,
		private readonly responsiveService: ResponsiveService,
		private readonly sensorService: SensorService,
		private readonly fpsService: FpsService
	) {}
	private readonly renderer = new Subject<WebGLRenderer>();
	private readonly scene = new Subject<SceneScene>();
	private readonly gameState = new ReplaySubject<ClientGameState>( 1 );
	private readonly destroyed = new Subject<true>();

	public showFps: boolean;

	private dirty = true;

	public ngOnInit() {
		this.showFps = this.fpsService.enabled;
		const clock = new Clock;

		const perspCamera = new PerspectiveCamera( 75, 1, 2, 20 );
		perspCamera.name = 'camera_persp';
		perspCamera.position.set( 0, -5, 15 );
		perspCamera.lookAt( 0, 0, 0 );
		perspCamera.updateProjectionMatrix();

		const orthoCamera = new OrthographicCamera( 0, 0, 0, 0, 1, 50 );
		orthoCamera.name = 'camera_ortho';
		orthoCamera.position.set( 0, 0, 10 );

		const activeCamera = orthoCamera;
		// const activeCamera = perspCamera;

		let orientationChanged = true;
		const orientation = new Quaternion;
		const sensor =
			this.sensorService.getAbsoluteOrientation()
			.pipe(
				map( xyzw => ( new Quaternion ).fromArray( xyzw ) ),
				catchError( () =>
					this.sensorService.getDeviceOrientation()
					.pipe( map( xyz => ( new Quaternion ).setFromEuler( ( new Euler ).fromArray( xyz ) ) ) )
				)
			);

		const moveThreshold = ( threshold: number ) => filter<Quaternion>( ( q, i ) => ( i === 0 ) || Math.abs( 1 - orientation.dot( q ) ) > threshold );

		const hasSensor = sensor.pipe(
			skip( 1 ),
			take( 1 ),
			map( () => true ),
			shareReplay( 1 ),
			takeUntil( this.destroyed )
		);

		sensor.pipe(
			moveThreshold( 0.01 ),
			switchMap( () =>
				sensor.pipe(
					takeUntil( sensor.pipe( debounceTime( 250 ) ) ),
					repeat( {
						delay: () => this.scene
	 				} )
				)
			),
			takeUntil( this.destroyed )
		).subscribe( q => {
			orientation.copy( q );
			orientationChanged = true;
		} );

		interval( 0, animationFrameScheduler )
		.pipe(
			takeUntil( hasSensor ),
			takeUntil( this.destroyed )
		)
		.subscribe( () => {
			const theta = Math.sin( performance.now() / 16_000 ) * Math.PI / 8;
			orientation.setFromAxisAngle( new Vector3( .3, .3, .3 ).normalize(), theta );
			orientationChanged = true;
		} );

		const actions = new Set<AnimationAction>();
		function fadeObj( obj: UserData<Object3D, FadeUserData>, firstRun: boolean, wasVisible: boolean, isVisible: boolean, shadow: boolean ) {
			if( !boardSettings.shadows.enabled ) shadow = false;
			const { fadeIn, fadeOut } = obj.userData.actions;
			if( firstRun || ( isVisible === wasVisible ) ) {
				actions.add( fadeIn.stop() );
				actions.add( fadeOut.stop() );
				obj.traverse( o => {
					o.visible = isVisible;
					o.castShadow = shadow && isVisible;
					if( o instanceof Mesh ) {
						for( const m of getArray( o.material ) ) {
							m.opacity = isVisible ? 1 : 0;
						}
					}
				} );
			} else {
				obj.traverse( o => {
					o.visible = true;
					o.castShadow = shadow && wasVisible;
					if( o instanceof Mesh ) {
						for( const m of getArray( o.material ) ) {
							m.opacity = wasVisible ? 1 : 0;
						}
					}
				} );
				const [ prevAction, nextAction ] = isVisible ?
					[ fadeOut, fadeIn ]
				:	[ fadeIn, fadeOut ];
				if( prevAction.isScheduled() ) actions.add( prevAction.stop() );
				actions.add( nextAction.stop().play() );
			}
		}

		combineLatest( [
			this.renderer,
			this.scene
		] )
		.pipe(
			filter( e => e.every( e => !!e ) ),
			switchMap( ( [ renderer, scene ] ) => {
				const composer = new EffectComposer( renderer );
				const renderPass = new RenderPass( scene, activeCamera );
				const fxaaPass = new ShaderPass( FXAAShader );
				const outputPass = new OutputPass();
				composer.addPass( renderPass );
				composer.addPass( fxaaPass );
				composer.addPass( outputPass );
				return interval( 0, animationFrameScheduler )
				.pipe( map( () => ( { composer, renderer, scene } ) ) );
			} ),
			takeUntil( this.destroyed )
		)
		.subscribe( ( { composer, renderer, scene } ) => {
			this.fpsService.beforeFrame();
			const delta = clock.getDelta();

			if( actions.size > 0 ) {
				if( ![ ...actions.values() ].some( a => a.isScheduled() ) ) {
					actions.clear();
				}
				this.dirty = true;
			}

			if( orientationChanged ) {
				scene.userData.lights.setRotationFromQuaternion( orientation );
				orientationChanged = false;
				this.dirty = true;
			}

			scene.userData.mixer.update( delta );

			if( this.dirty ) {
				const fxaaPass = composer.passes[ 1 ] as ShaderPass;
				fxaaPass.uniforms[ 'resolution' ].value.x = 1 / ( renderer.domElement.width * renderer.getPixelRatio() );
				fxaaPass.uniforms[ 'resolution' ].value.y = 1 / ( renderer.domElement.height * renderer.getPixelRatio() );
				composer.render( delta );
				this.dirty = false;
			}

			this.fpsService.afterFrame();
		} );

		const mouseEvents =
		combineLatest( [ this.scene, this.renderer ] )
		.pipe(
			filter( e => e.every( e => !!e ) ),
			switchMap( ( [ scene, renderer ] ) => {
				const { domElement: canvas } = renderer;
				return merge(
					of( 'mousemove', 'touchmove', 'click' )
					.pipe(
						mergeMap( type =>
							fromEvent<MouseEvent>( canvas, type, { passive: true } )
							.pipe(
								map( ( { clientX, clientY } ) => ( { type, clientX, clientY } ) )
							)
						)
					),
					of( 'touchend' )
					.pipe(
						mergeMap( type =>
							fromEvent<TouchEvent>( canvas, type, { passive: true } )
							.pipe(
								mergeMap( ( { touches } ) => touches ),
								map( ( { clientX, clientY } ) => ( { type, clientX, clientY } ) )
							)
						)
					)
				)
				.pipe(
					map( ( { clientX, clientY, type } ) => {
						const bounds = canvas.getBoundingClientRect();
						const x = clientX - bounds.left;
						const y = clientY - bounds.top;
						const point = { x: ( x / bounds.width * 2 - 1 ), y: ( y / bounds.height ) * -2 + 1 };
						const raycaster = new Raycaster;
						raycaster.setFromCamera( new Vector2( point.x, point.y ), activeCamera );
						const [ position = null ] =
							raycaster
							.intersectObjects( scene.children, true )
							.map( i => ( i.object as MeshBoard ).userData.position )
							.filter( e => !!e );

						return {
							type,
							point,
							position
						};
					} )
				);
			} )
		);

		mouseEvents
		.pipe( filter( e => [ 'mousemove', 'touchmove' ].includes( e.type ) ) )
		.subscribe( ( { position } ) => {
			this.mousemove.emit( { position } );
		} );

		mouseEvents
		.pipe( filter( e => [ 'touchend', 'click' ].includes( e.type ) ) )
		.subscribe( ( { position } ) => {
			this.click.emit( { position } );
		} );

		async function getScene( size: Size ) {
			const { width, height } = size;
			const scene = new Scene() as SceneScene;
			scene.name = 'scene';
			const lights = new Group;
			lights.name = 'lights';
			scene.add( lights );
			const fixedLights = new Group;
			fixedLights.name = 'fixedLights';
			scene.add( fixedLights );
			const mixer = new AnimationMixer( scene );
			scene.userData.mixer = mixer;
			scene.userData.lights = lights;
			scene.userData.fixedLights = fixedLights;
			const markerProto = await getMarker();
			const pieceProto = await getPiece();
			const flipClip = generateFlipClip( pieceProto );
			const pieceFadeInClip = generateFadeClip( pieceProto, true, boardSettings.shadows.enabled && boardSettings.shadows.pieces );
			const pieceFadeOutClip = generateFadeClip( pieceProto, false, boardSettings.shadows.enabled && boardSettings.shadows.pieces );
			const markerFadeInClip = generateFadeClip( markerProto, true, boardSettings.shadows.enabled && boardSettings.shadows.markers );
			const markerFadeOutClip = generateFadeClip( markerProto, false, boardSettings.shadows.enabled && boardSettings.shadows.markers );

			let helpers: Group;
			if( boardSettings.helpers.enabled ) {
				helpers = new Group;
				helpers.name = 'helpers';
				scene.add( helpers );
				if( boardSettings.helpers.axes ) {
					helpers.add( new AxesHelper );
				}
				if( boardSettings.helpers.grid ) {
					const grid = new Group;
					helpers.add( grid );
					grid.add(
						new GridHelper( 16, 16 )
					);
					grid.add(
						new GridHelper( 16, 16 )
						.rotateX( Math.PI * .5 )
					);
				}
			}

			function addObjectHelpers( obj: Object3D ) {
				if( !boardSettings.helpers.enabled ) return;
				if( boardSettings.helpers.box ) helpers.add( new BoxHelper( obj ) );
			}
			const boardRoot = new Group;
			boardRoot.name = 'board_root';
			scene.add( boardRoot );
			const borderRoot = new Group;
			borderRoot.name = 'border_root';
			boardRoot.add( borderRoot );
			borderRoot.position.set( 0, 0, .025 );
			const border = await getBorder();
			border.receiveShadow = boardSettings.shadows.enabled;
			enableShadows( border, boardSettings.shadows.border );
			border.name = 'border';
			borderRoot.add( border );
			const borderSize = { x: width * borderScale, y: height * borderScale };
			border.scale.set( width + borderSize.x * 2, height + borderSize.y * 2, 1 );
			if( boardSettings.helpers.board ) addObjectHelpers( border );
			const squareObjects = scene.userData.squareObjects = new Grid<SquareObjects>( width, height );
			const squares = new Group;
			boardRoot.add( squares );
			for( let y = 0; y < height; ++y )
			for( let x = 0; x < width; ++x ) {
				const xl = 'ABCDEFGHJIJKLMNOPQRSTUVWXYZ'.charAt( x );
				const yl = '1234567890'.charAt( y );
				const square = new Group;
				square.name = `square_${xl}${yl}`;
				square.position.set( ( width - 1 ) * -.5 + x, ( height - 1 ) * .5 - y, 0 );
				squares.add( square );
				globalThis[ square.name ] = square;
				square.updateMatrixWorld();
				const board = await getBoard( { x, y }, size ) as unknown as MeshBoard;
				board.name = `board_${xl}${yl}`;
				board.receiveShadow = boardSettings.shadows.enabled;
				board.userData.position = { x, y };
				square.add( board );
				globalThis[ board.name ] = board;
				if( boardSettings.helpers.board ) addObjectHelpers( board );
				const piece = await getPiece() as unknown as MeshPiece;
				piece.name = `piece_${xl}${yl}`;
				globalThis[ piece.name ] = piece;
				setPieceTopColor( piece, new Color( 0, 1, 0 ) );
				enableShadows( piece, boardSettings.shadows.pieces );
				piece.userData.actions = {
					flip: mixer.clipAction( flipClip, piece ).setLoop( LoopOnce, 1 ),
					fadeIn: mixer.clipAction( pieceFadeInClip, piece ).setLoop( LoopOnce, 1 ),
					fadeOut: mixer.clipAction( pieceFadeOutClip, piece ).setLoop( LoopOnce, 1 )
				};
				piece.userData.actions.flip.zeroSlopeAtStart = false;
				piece.userData.actions.flip.zeroSlopeAtEnd = false;
				piece.userData.actions.flip.clampWhenFinished = true;
				piece.userData.actions.fadeIn.zeroSlopeAtStart = false;
				piece.userData.actions.fadeIn.zeroSlopeAtEnd = false;
				piece.userData.actions.fadeIn.clampWhenFinished = true;
				piece.userData.actions.fadeOut.zeroSlopeAtStart = false;
				piece.userData.actions.fadeOut.zeroSlopeAtEnd = false;
				piece.userData.actions.fadeOut.clampWhenFinished = true;
				square.add( piece );
				if( boardSettings.helpers.pieces ) addObjectHelpers( piece );
				const marker = await getMarker() as unknown as MeshMarker;
				enableShadows( marker, boardSettings.shadows.markers );
				square.add( marker );
				marker.name = `marker_${xl}${yl}`;
				globalThis[ marker.name ] = marker;
				marker.userData.actions = {
					fadeIn: mixer.clipAction( markerFadeInClip, marker ).setLoop( LoopOnce, 1 ),
					fadeOut: mixer.clipAction( markerFadeOutClip, marker ).setLoop( LoopOnce, 1 )
				};
				marker.userData.actions.fadeIn.zeroSlopeAtStart = false;
				marker.userData.actions.fadeIn.zeroSlopeAtEnd = false;
				marker.userData.actions.fadeIn.clampWhenFinished = true;
				marker.userData.actions.fadeOut.zeroSlopeAtStart = false;
				marker.userData.actions.fadeOut.zeroSlopeAtEnd = false;
				marker.userData.actions.fadeOut.clampWhenFinished = true;
				if( boardSettings.helpers.markers ) addObjectHelpers( marker );
				squareObjects.set( { x, y }, { board, marker, piece } );
			}

			const texts = new Group;
			borderRoot.add( texts );
			texts.position.set( 0, 0, .1 );
			texts.updateMatrixWorld();

			for( let x = 0; x < width; ++x ) {
				const center = {
					x: ( width - 1 ) * -.5 + x,
					y: ( height + borderSize.y ) * .5
				};

				const symbol = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.charAt( x );
				const textMesh = await getText( symbol );
				enableShadows( textMesh, boardSettings.shadows.text );
				textMesh.name = `label_${symbol}`;
				textMesh.scale.setScalar( borderSize.y * .5 );
				textMesh.position.set( center.x, center.y, 0 );
				texts.add( textMesh );
				globalThis[ textMesh.name ] = textMesh;
				if( boardSettings.helpers.text ) addObjectHelpers( textMesh );
				const textMeshDown = textMesh.clone();
				textMeshDown.rotateZ( Math.PI );
				textMeshDown.name += '_down';
				textMeshDown.position.add( new Vector3( 0, -height - borderSize.y, 0 ) );
				texts.add( textMeshDown );
				globalThis[ textMeshDown.name ] = textMeshDown;
				if( boardSettings.helpers.text ) addObjectHelpers( textMeshDown );
			}

			for( let y = 0; y < height; ++y ) {
				const center = {
					x: ( width + borderSize.x ) * -.5,
					y: ( height - 1 ) * .5 - y
				};

				const symbol = '1234567890'.charAt( y );
				const textMesh = await getText( symbol );
				enableShadows( textMesh, boardSettings.shadows.text );
				textMesh.name = `label_${symbol}`;
				textMesh.scale.setScalar( borderSize.x * .5 );
				textMesh.position.set( center.x, center.y, 0 );
				texts.add( textMesh );
				globalThis[ textMesh.name ] = textMesh;
				if( boardSettings.helpers.text ) addObjectHelpers( textMesh );
				const textMeshDown = textMesh.clone();
				textMeshDown.rotateZ( Math.PI );
				textMeshDown.name += '_down';
				textMeshDown.position.add( new Vector3( width + borderSize.x, 0, 0 ) );
				texts.add( textMeshDown );
				globalThis[ textMeshDown.name ] = textMeshDown;
				if( boardSettings.helpers.text ) addObjectHelpers( textMeshDown );
			}

			orthoCamera.left = width * -( .5 + borderScale );
			orthoCamera.right = width * ( .5 + borderScale );
			orthoCamera.top = height * ( .5 + borderScale );
			orthoCamera.bottom = height * -( .5 + borderScale );
			orthoCamera.updateProjectionMatrix();
			if( helpers && boardSettings.helpers.camera ) {
				helpers.add( new CameraHelper( orthoCamera ) );
			}

			let lightIndex = 0;
			for( const lightDefinition of boardSettings.lights ) {
				let light: Light;
				let shadow: DirectionalLightShadow|PointLightShadow|SpotLightShadow;
				let shadowTarget = new Vector3( 0, 0, 0 );
				const castShadow = boardSettings.shadows.enabled && ( lightDefinition.shadow !== false );
				switch( lightDefinition.type ) {
				case 'ambient':
					light = new AmbientLight( hslToColor( lightDefinition.color ), lightDefinition.intensity );
					break;
				case 'directional': {
					const directionalLight = light = new DirectionalLight( hslToColor( lightDefinition.color ), lightDefinition.intensity );
					directionalLight.target.position.set( ...lightDefinition.target );
					directionalLight.target.updateMatrixWorld( false );
					directionalLight.lookAt( directionalLight.target.position );
					shadow = directionalLight.shadow;
					if( helpers && boardSettings.helpers.lights && lightDefinition.helper !== false ) {
						helpers.add( new DirectionalLightHelper( directionalLight ) );
					}
					break;
				}
				case 'hemisphere': {
					const hemisphereLight = light = new HemisphereLight( hslToColor( lightDefinition.skyColor ), hslToColor( lightDefinition.groundColor ), lightDefinition.intensity );
					if( helpers && boardSettings.helpers.lights && lightDefinition.helper !== false ) {
						helpers.add( new HemisphereLightHelper( hemisphereLight, 1 ) );
					}
					break;
				}
				case 'point': {
					const pointLight = light = new PointLight( hslToColor( lightDefinition.color ), lightDefinition.intensity, lightDefinition.distance ?? 0, lightDefinition.decay ?? 2 );
					shadow = pointLight.shadow;
					if( helpers && boardSettings.helpers.lights && lightDefinition.helper !== false ) {
						helpers.add( new PointLightHelper( pointLight ) );
					}
					break;
				}
				case 'spotlight': {
					const spotLight = light = new SpotLight( hslToColor( lightDefinition.color ), lightDefinition.intensity, lightDefinition.distance, lightDefinition.angle / 180 * Math.PI, lightDefinition.penumbra ?? 0, lightDefinition.decay ?? 2 );
					if( lightDefinition.target ) {
						spotLight.target.position.set( ...lightDefinition.target );
					}
					spotLight.target.updateMatrixWorld( false );
					scene.add( spotLight.target );
					shadow = spotLight.shadow;
					shadowTarget = spotLight.target.position;
					spotLight.shadow.camera.fov = lightDefinition.angle;
					if( helpers && boardSettings.helpers.lights && lightDefinition.helper !== false ) {
						helpers.add( new SpotLightHelper( spotLight ) );
					}
					break;
				}
				default: throw new Error( 'Unknown light type' );
				}
				if( lightDefinition.position ) {
					light.position.set( ...lightDefinition.position );
				}
				light.updateMatrixWorld( false );
				if( castShadow && shadow ) {
					light.castShadow = true;
					shadow.mapSize.width = boardSettings.shadows.mapSize[ 0 ];
					shadow.mapSize.height = boardSettings.shadows.mapSize[ 1 ];
					shadow.camera.position.copy( light.position );
					shadow.camera.lookAt( shadowTarget );
					shadow.camera.updateMatrixWorld( false );
					shadow.camera.updateProjectionMatrix();
				}
				light.name = `light_${++lightIndex}_${lightDefinition.type}`;
				globalThis[ light.name ] = light;
				if( lightDefinition.fixed ) {
					fixedLights.add( light );
				} else {
					lights.add( light );
				}
			}
			return scene;
		}

		this.gameState
		.pipe(
			distinctUntilChanged(),
			scan<ClientGameState, Promise<{ gameState: ClientGameState|null, scene: SceneScene|null }>>(
				async ( oldState, gameState ) => {
					let { gameState: oldGameState, scene } = await oldState;
					if( !scene || !oldGameState || gameState.size.width !== oldGameState.size.width || gameState.size.height !== oldGameState.size.height ) {
						this.isLoading = true;
						this.changeDetectorRef.markForCheck();
						oldGameState = null;
						if( orthoCamera.parent ) orthoCamera.parent.remove( orthoCamera );
						if( perspCamera.parent ) perspCamera.parent.remove( perspCamera );
						if( scene ) disposeScene( scene );
						scene = await getScene( gameState.size );
						disposeOffscreenRenderer();
						scene.add( orthoCamera );
						scene.add( perspCamera );
						this.isLoading = false;
						this.changeDetectorRef.markForCheck();
					}
					const board = Board.fromGameState( gameState );
					const oldBoard = oldGameState && Board.fromGameState( oldGameState );
					const { lastMove, size: { width, height } } = gameState;
					const { mixer, squareObjects } = scene.userData;

					for( let y = 0; y < height; ++y )
					for( let x = 0; x < width; ++x ) {
						const { piece: pieceObj, marker: markerObj } = squareObjects.get( { x, y } );
						const square = board.get( { x, y } );
						const oldSquare = oldBoard && oldBoard.get( { x, y } );

						fadeObj(
							markerObj,
							!oldGameState,
							!!( oldGameState && oldGameState.lastMove && ( oldGameState.lastMove.x === x ) && ( oldGameState.lastMove.y === y ) ),
							!!( lastMove && ( lastMove.x === x ) && ( lastMove.y === y ) ),
							boardSettings.shadows.markers
						);

						fadeObj(
							pieceObj,
							!oldGameState,
							!!( oldSquare && ( oldSquare.color != null ) ),
							square.color != null,
							boardSettings.shadows.pieces
						);

						if( square.color != null ) {
							setPieceTopColor( pieceObj, hslToColor( colors[ this.colors[ square.color ] ].color ) );

							if( ( oldSquare?.color != null ) && ( oldSquare.color !== square.color ) ) {
								setPieceBottomColor( pieceObj, hslToColor( colors[ this.colors[ oldSquare.color ] ].color ) );
								const { lastMove } = gameState;
								let offsetTime = 0;
								if( lastMove ) {
									offsetTime = ( distance( lastMove, square.position ) - 1 ) * .2;
								}
								pieceObj.rotation.set( Math.PI * 2, Math.PI, Math.PI * .5 );
								actions.add( pieceObj.userData.actions.flip.stop().startAt( mixer.time + offsetTime ).play() );
							}
						}
					}
					this.dirty = true;
					return { gameState, scene };
				}, Promise.resolve( { gameState: null, scene: null } )
			),
			switchMap( async s => ( await s ).scene )
		)
		.subscribe( this.scene );

		combineLatest( [
			this.scene,
			this.renderer
		] ).pipe( sample( this.destroyed ) )
		.subscribe( ( [ scene, renderer ] ) => {
			if( scene ) disposeScene( scene );
			if( renderer ) {
				renderer.dispose();
			}
		} );
	}

	public ngAfterViewInit() {
		const canvas = this.canvasElementRef.nativeElement as HTMLCanvasElement;
		canvas.style.width = '100%';

		const renderer = new WebGLRenderer( {
			antialias: true,
			canvas,
			precision: 'highp'
		} );
		renderer.debug.checkShaderErrors = process.env.DEBUG === 'true';
		renderer.toneMapping = ReinhardToneMapping;
		renderer.outputColorSpace = DisplayP3ColorSpace;

		combineLatest( [
			this.responsiveService.getElementClientSize( canvas ),
			this.gameState
		] )
		.pipe( takeUntil( this.destroyed ) )
		.subscribe( ( [ { width }, gameState ] ) => {
			const boardRatio = ( gameState.size.height + 1 ) / ( gameState.size.width + 1 );
			const height = width * boardRatio;
			const pixelRatio = boardSettings.useDevicePixelRatio ? ( devicePixelRatio ?? 1 ) : 1;
			renderer.setDrawingBufferSize( width, height, pixelRatio );
			this.dirty = true;
		} );

		if( boardSettings.shadows.enabled ) {
			renderer.shadowMap.enabled = true;
			switch( boardSettings.shadows.type ) {
			case 'basic':
				renderer.shadowMap.type = BasicShadowMap;
				break;
			case 'pcf':
				renderer.shadowMap.type = PCFShadowMap;
				break;
			case 'pcfsoft':
				renderer.shadowMap.type = PCFSoftShadowMap;
				break;
			case 'vsm':
				renderer.shadowMap.type = VSMShadowMap;
				break;
			default: throw new Error( 'Unknown shadow map type' );
			}
		} else {
			renderer.shadowMap.enabled = false;
		}
		this.renderer.next( renderer );
	}

	public ngOnChanges() {
		this.gameState.next( this.gameStateValue );
	}

	public ngOnDestroy() {
		this.renderer.complete();
		this.scene.complete();
		this.gameState.complete();
		this.destroyed.next( true );
		this.destroyed.complete();
		disposeOffscreenRenderer();
	}

	public isLoading = true;

	@ViewChild( 'canvasElement', { static: false } )
	private canvasElementRef: ElementRef;

	@Input()
	public colors: string[] = [];

	public board: null|Board = null;

	@Input( 'gameState' )
	public gameStateValue: null|ClientGameState = null;

	@Output()
	public click = new EventEmitter<BoardMouseEvent>();

	@Output()
	public mousemove = new EventEmitter<BoardMouseEvent>();
}
