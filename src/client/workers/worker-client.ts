import { fromEvent, Observable, from, throwError, merge } from 'rxjs';
import { share, filter, map, switchMap, mergeMap } from 'rxjs/operators';
import { MessageMap } from '~client/worker-messages';

type Response<T> = Observable<T>|Promise<T>|ReadonlyArray<T>;
type ResponsePayload<T extends object> = T|readonly [ T, readonly Transferable[] ];

function getPorts( e: MessageEvent ) {
	return Array.from( new Set<MessagePort>( [ ( e as any ).port, ...( e.ports ?? [] ) ].filter( e => e != null ) ).values() );
}

interface WorkerClientOptions {}
export class WorkerClient<TMessageMap extends MessageMap = {}> {
	private readonly message$: Observable<MessageEvent>;

	public constructor( options: WorkerClientOptions ) {
		this.message$ =
			fromEvent<MessageEvent>( self, 'message' )
			.pipe( share() );
	}

	public handleMessage<
		TMessageKey extends keyof TMessageMap,
		TRequestKey extends keyof TMessageMap[TMessageKey] & 'request',
		TInKey extends keyof TMessageMap[TMessageKey] & 'in',
		TOutKey extends keyof TMessageMap[TMessageKey] & 'out'
	>(
		type: TMessageKey,
		fn: ( request: ( TMessageMap[TMessageKey][TRequestKey] extends never ? {} : TMessageMap[TMessageKey][TRequestKey] ) & { readonly type: TMessageKey; }, input: Observable<TMessageMap[TMessageKey][TInKey]> ) => Response<ResponsePayload<TMessageMap[TMessageKey][TOutKey] & {}>>
	) {
		this.message$.pipe(
			filter( d => d.data.type === type ),
			mergeMap( e => getPorts( e ).map( port => ( { data: e.data, port } ) ) )
		).subscribe( ( { port, data } ) => {
			const input$ =
				merge(
					fromEvent<MessageEvent>( port, 'message' ),
					fromEvent<ErrorEvent>( port, 'messageerror' )
					.pipe( switchMap( e => throwError( () => e ) ) )
				).pipe(
					map( e => e.data ),
					share()
				);

			from( fn( data, input$ ) )
			.subscribe( {
				next: data => {
					if( Array.isArray( data ) ) {
						const [ d, t ] = data as any;
						port.postMessage( d, t );
					} else {
						port.postMessage( data );
					}
				},
				error: err => {
					console.error( err );
					port.close();
				},
				complete: () => {
					port.close();
				}
			} );
		} );
	}
}
