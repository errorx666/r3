import { WorkerClient } from './worker-client';
import { from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { buildBorderMaps, buildPieceFaceMaps, serializeDataTexture, getText, serializeBillboard, buildBoardMaps, getMarker } from '~client/builder';
import { BoardWorkerMessageMap } from '~client/worker-messages';

const workerClient = new WorkerClient<BoardWorkerMessageMap>( {} );

workerClient.handleMessage( 'ready', async () => {
	return { ready: true };
} );

workerClient.handleMessage( 'board-maps', async () => {
	const { bumpMap, metalnessMap, roughnessMap } = buildBoardMaps();
	const payload = {
		buildId: BUILD_ID,
		bumpMap: serializeDataTexture( bumpMap ),
		metalnessMap: serializeDataTexture( metalnessMap ),
		roughnessMap: serializeDataTexture( roughnessMap )
	};
	bumpMap.dispose();
	metalnessMap.dispose();
	roughnessMap.dispose();
	return [
		payload,
		[
			payload.bumpMap.image.data.buffer,
			payload.metalnessMap.image.data.buffer,
			payload.roughnessMap.image.data.buffer
		]
	] as readonly [ typeof payload, readonly Transferable[] ];
} );

workerClient.handleMessage( 'border-maps', async () => {
	const { alphaMap, faceMap, map } = buildBorderMaps();
	const payload = {
		buildId: BUILD_ID,
		alphaMap: serializeDataTexture( alphaMap ),
		faceMap: serializeDataTexture( faceMap ),
		map: serializeDataTexture( map )
	};
	alphaMap.dispose();
	faceMap.dispose();
	map.dispose();
	return [
		payload,
		[
			payload.alphaMap.image.data.buffer,
			payload.faceMap.image.data.buffer,
			payload.map.image.data.buffer
		]
	] as readonly [ typeof payload, readonly Transferable[] ];
} );

workerClient.handleMessage( 'marker', async () => {
	const billboard = await getMarker();
	const payload = serializeBillboard( billboard );
	return [
		payload,
		[
			payload.alphaMap.image.data.buffer,
			payload.normalMap.image.data.buffer
		]
	] as readonly [ typeof payload, readonly Transferable[] ];
} );


workerClient.handleMessage( 'piece-face-maps', async () => {
	const { alphaMap, bumpMap } = buildPieceFaceMaps();
	const payload = {
		buildId: BUILD_ID,
		alphaMap: serializeDataTexture( alphaMap ),
		bumpMap: serializeDataTexture( bumpMap )
	};
	alphaMap.dispose();
	bumpMap.dispose();
	return [
		payload,
		[
			payload.alphaMap.image.data.buffer,
			payload.bumpMap.image.data.buffer
		]
	] as readonly [ typeof payload, readonly Transferable[] ];
} );

workerClient.handleMessage( 'text', async ( { symbol } ) => {
	const billboard = await getText( symbol );
	const payload = serializeBillboard( billboard );
	return [
		payload,
		[
			payload.alphaMap.image.data.buffer,
			payload.normalMap.image.data.buffer
		]
	] as readonly [ typeof payload, readonly Transferable[] ];
} );

from( '1A2B3C4D5E6F7G8H9I0J'.split( '' ) )
.pipe( mergeMap( getText ) )
.subscribe();
