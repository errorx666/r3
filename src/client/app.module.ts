/* eslint key-spacing: "off", no-multi-spaces: "off" */

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';

import { BoardComponent } from './board.component';
import { ChatComponent } from './chat.component';
import { FpsComponent } from './fps.component';
import { FpsService } from './fps.service';
import { GameComponent } from './game.component';
import { GameService } from './game.service';
import { LobbyComponent } from './lobby.component';
import { MarkdownPipe } from './markdown.pipe';
import { ModalModule } from './modal/modal.module';
import { NavigationComponent } from './navigation.component';
import { R3Component } from './r3.component';
import { AutoScrollDirective } from './auto-scroll.directive';
import { RoomComponent } from './room.component';
import { ScoreboardComponent } from './scoreboard.component';
import { ResponsiveService } from './responsive.service';
import { RoomService } from './room.service';
import { SessionService } from './session.service';
import { SocketService } from './socket.service';
import { ValidatorModule } from './validator/validator.module';
import { SensorService } from './sensor.service';
import { registerFonts } from './font-awesome';

@NgModule( {
	imports:      [ BrowserAnimationsModule, BrowserModule, CommonModule, FormsModule, ModalModule, FontAwesomeModule, ValidatorModule ],
	declarations: [ BoardComponent, ChatComponent, GameComponent, LobbyComponent, MarkdownPipe, NavigationComponent, R3Component, RoomComponent, ScoreboardComponent, AutoScrollDirective, FpsComponent ],
	providers:    [ FpsService, GameService, ResponsiveService, RoomService, SessionService, SocketService, SensorService ],
	bootstrap:    [ R3Component ]
} )
export class AppModule {
	public constructor( library: FaIconLibrary ) {
		registerFonts( library );
	}
}
