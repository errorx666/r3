declare module '~data/responsive.yaml' {
	export const breakpoints: {
		readonly xs: number;
		readonly sm: number;
		readonly md: number;
		readonly lg: number;
		readonly xl: number;
	};
}

type AmbientLightDefinition = {
	type: 'ambient';
	color: NumberTriple;
	intensity: number;
};

type PointLightDefinition = {
	type: 'point';
	color: NumberTriple;
	intensity: number;
	position: NumberTriple;
	distance?: number;
	decay?: number;
	shadow?: boolean;
	helper?: boolean;
	fixed?: boolean;
};

type DirectionalLightDefinition = {
	type: 'directional';
	color: NumberTriple;
	target: NumberTriple;
	intensity: number;
	shadow?: boolean;
	helper?: boolean;
	fixed?: boolean;
};

type SpotlightDefinition = {
	type: 'spotlight';
	color: NumberTriple;
	intensity: number;
	position: NumberTriple;
	target: NumberTriple;
	decay: number;
	penumbra: number;
	distance: number;
	angle: number;
	shadow?: boolean;
	helper?: boolean;
	fixed?: boolean;
};

type HemisphereLightDefinition = {
	type: 'hemisphere';
	skyColor: NumberTriple;
	groundColor: NumberTriple;
	intensity: number;
	position: NumberTriple;
	helper?: boolean;
	fixed?: boolean;
};

type LightDefinition = ( AmbientLightDefinition|PointLightDefinition|DirectionalLightDefinition|SpotlightDefinition|HemisphereLightDefinition ) & {
	helper?: boolean|undefined;
	shadow?: boolean|undefined;
	position?: NumberTriple|undefined;
	fixed?: boolean|undefined;
};

declare module '~data/app.config.yaml' {
	export const showFps: boolean;
	export const showDevTools: boolean;
}

declare module '~data/board.yaml' {
	import { TextGeometryParameters } from 'three';

	declare const enum ShadowMapType {
		basic = 'basic',
		pcf = 'pcf',
		pcfsoft = 'pcfsoft',
		vsm = 'vsm'
	}

	export const lights: ReadonlyArray<Readonly<LightDefinition>>;
	export const helpers: {
		readonly enabled: boolean;
		readonly axes: boolean;
		readonly board: boolean;
		readonly border: boolean;
		readonly box: boolean;
		readonly camera: boolean;
		readonly grid: boolean;
		readonly lights: boolean;
		readonly markers: boolean;
		readonly pieces: boolean;
		readonly text: boolean;
	};
	export const shadows: {
		readonly enabled: boolean;
		readonly type: ShadowMapType;
		readonly border: boolean;
		readonly pieces: boolean;
		readonly markers: boolean;
		readonly text: boolean;
		readonly mapSize: readonly [ number, number ];
	};
	export const textGeometry: Readonly<TextGeometryParameters>;
	export const useDevicePixelRatio: boolean;
	export const mapSize: number;
	export const useBackgroundRenderer: boolean;
}

declare module '~data/dompurify.config.yaml' {
	import { Config } from 'dompurify';
	const config: Config;
	export default config;
}

declare module '~data/sensor.config.yaml' {
	export const absoluteOrientation: SensorOptions;
	export const gyroscope: SensorOptions;
}
