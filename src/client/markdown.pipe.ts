import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { marked } from 'marked';
import DOMPurify, { sanitize } from 'dompurify';
import sanitizeConfig from '~data/dompurify.config.yaml';

DOMPurify.addHook( 'beforeSanitizeElements', ( currentNode, data, config ) => {
	if( currentNode.tagName === 'TABLE' ) {
		if( !currentNode.classList.contains( 'table' ) ) {
			currentNode.classList.add( 'table' );
		}
	}
} );

@Pipe( { name: 'markdown' } )
export class MarkdownPipe implements PipeTransform {
	constructor( private readonly sanitizer: DomSanitizer ) { }

	public transform( markdown: string ) {
		const { sanitizer } = this;
		const unsafeHtml = marked( markdown, { async: false, breaks: true, gfm: true } ) as string;
		const saferHtml = sanitize( unsafeHtml,
			{ ...sanitizeConfig,
				CUSTOM_ELEMENT_HANDLING: {

				}
			}
		);
		const safeHtml = sanitizer.sanitize( SecurityContext.HTML, saferHtml );
		return safeHtml;
	}
}
