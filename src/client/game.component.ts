import { rulesStandard } from '~rule-sets';

import { combineLatest, Subject } from 'rxjs';

import { ChangeDetectorRef, Component, OnInit, ViewChild, OnDestroy, ChangeDetectionStrategy, AfterViewInit, HostListener } from '@angular/core';

import { GameService } from './game.service';
import { RoomService } from './room.service';
import { map, takeUntil, filter, take } from 'rxjs/operators';
import { ModalNewGameComponent } from './modal/new-game.component';
import { colors } from '~data/colors.yaml';
import { SessionService } from '~client/session.service';

const rules = rulesStandard;

@Component( {
	selector: 'game',
	templateUrl: './game.component.html',
	styleUrls: [ './game.component.css' ],
	changeDetection: ChangeDetectionStrategy.OnPush
} )
export class GameComponent implements OnInit, AfterViewInit, OnDestroy {
	constructor(
		private readonly changeDetector: ChangeDetectorRef,
		private readonly gameService: GameService,
		private readonly roomService: RoomService,
		private readonly sessionService: SessionService
	) {}

	@ViewChild( 'newGameModal', { static: true } )
	public newGameModal: ModalNewGameComponent;

	public ngOnInit() {
		const { destroyed, gameService, roomService, sessionService } = this;

		roomService.getCurrentRoomId()
		.pipe( takeUntil( destroyed ) )
		.subscribe( roomId => {
			this.roomId = roomId;
			this.changeDetector.markForCheck();
		} );

		combineLatest( [
			gameService.getGames(),
			roomService.getCurrentRoom(),
			roomService.getCurrentRoomSessions(),
			sessionService.getSessionId()
		] )
		.pipe(
			map( ( [ games, room, roomSessions, sessionId ] ) => ( {
				game: room ? games.get( room.gameId ) : null,
				roomSessions,
				sessionId
			} ) ),
			takeUntil( destroyed )
		)
		.subscribe( ( { game, roomSessions, sessionId } ) => {
			this.game = game;
			if( game ) {
				const gameState = this.gameState = game.gameStates.slice( -1 )[ 0 ];
				const c = this.colors = [ ...game.colors ];
				if( gameState.turn == null ) {
					this.canMove = false;
					this.turn = null;
				} else {
					const canMoveSession = roomSessions.filter( rs => rs.seats.includes( gameState.turn ) )[ 0 ];
					this.canMove = !canMoveSession || canMoveSession.sessionId === sessionId;
					this.turn = colors[ c[ gameState.turn ] ].displayName;
				}
			} else {
				this.canMove = false;
				this.gameState = null;
				this.turn = null;
			}
			this.changeDetector.markForCheck();
		} );
	}

	public ngAfterViewInit() {
		this.roomService.getCurrentRoom()
		.pipe(
			filter( r => r && !r.gameId ),
			take( 1 ),
			takeUntil( this.destroyed )
		)
		.subscribe( room => {
			this.newGameModal.show( room.id );
		} );

		this.roomService.getCurrentRoom()
		.pipe(
			filter( r => r && !!r.gameId ),
			takeUntil( this.destroyed )
		).subscribe( () => {
			this.newGameModal.hide();
		} );
	}

	public ngOnDestroy() {
		this.destroyed.next( true );
		this.destroyed.complete();
	}

	public canMove = false;
	public colors = null as string[];
	public turn = null as string|null;
	public roomId = null as string|null;
	public game = null as ClientGame|null;
	public gameState = null as ClientGameState|null;
	public lastMove = null as Point|null;
	public rules = null as Rules|null;

	private readonly destroyed = new Subject<true>();

	public onMouseMove( { position }: BoardMouseEvent ) {
		if( !position ) return;
		const { canMove, gameState } = this;
		document.documentElement.style.cursor =
			( rules.isGameOver( gameState )
			||	( canMove && rules.isValid( gameState, position, gameState.turn ) )
			) ? 'pointer' : null;
	}

	public onClick( { position }: BoardMouseEvent ) {
		if( !position ) return;
		const { canMove, roomId, gameState, gameService } = this;
		if( !rules.isGameOver( gameState ) && canMove && rules.isValid( gameState, position, gameState.turn ) ) {
			gameService.makeMove( roomId, position );
			document.documentElement.style.cursor = null;
		}
	}

	@HostListener( 'click', [ '$event.target' ] )
	public onBodyClick( target: Element ) {
		const { game, gameState, newGameModal, roomId } = this;
		if( target && newGameModal.hostElement.nativeElement.contains( target ) ) return;

		if( !game || rules.isGameOver( gameState ) ) {
			newGameModal.show( roomId );
		}
	}
}
