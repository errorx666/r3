import { Component, AfterViewInit, ViewChild, ElementRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { FpsService } from './fps.service';

import { firstValueFrom, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Component( {
	selector: 'FpsPanel',
	templateUrl: './fps.component.html',
	styleUrls: [ './fps.component.scss' ],
	changeDetection: ChangeDetectionStrategy.OnPush
} )
export class FpsComponent implements AfterViewInit, OnDestroy {
	public constructor(
		private readonly fpsService: FpsService
	) {}

	@ViewChild( 'fpsPlaceholder', { static: false } )
	public placeholder: ElementRef;

	private readonly destroyed = new Subject<true>();

	public ngAfterViewInit() {
		( async () => {
			const domElement = await Promise.race( [
				this.fpsService.domElement,
				firstValueFrom( this.destroyed.pipe( map( () => null ) ) )
			] );
			if( !domElement ) return;
			domElement.removeAttribute( 'style' );
			this.placeholder.nativeElement.appendChild( domElement );
		} )();
	}

	public ngOnDestroy() {
		this.destroyed.next( true );
		this.destroyed.complete();
	}
}
