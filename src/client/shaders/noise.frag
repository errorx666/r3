@import ./util;

varying vec3 vPosition;
uniform vec2 range;
uniform vec3 scale;
uniform vec3 color;

void main() {
	vec3 pos = vPosition * scale;
	float n = snoise( pos ) * ( range.y - range.x ) + range.x;
	vec3 c = color * n;
	gl_FragColor =  vec4( c.x, c.y, c.z, 1.0 );
}
