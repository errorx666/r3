// source: https://2pha.com/demos/threejs/shaders/wood_grain.html

@import ./util;

uniform vec3 color1;
uniform vec3 color2;
uniform float frequency;
uniform float noiseScale;
uniform float ringScale;
varying vec3 vPosition;

void main() {
	float n = snoise( vPosition * 2.0 );
	float ring = fract( frequency * vPosition.z + noiseScale * n );
	ring *= 4.0 * ( 1.0 - ring );
	float lrp = pow( ring, ringScale ) + n;
	vec3 base = mix( color1, color2, lrp );
	gl_FragColor = vec4( base, 1.0 );
}
