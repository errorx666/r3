import { combineLatest, Subject } from 'rxjs';
import { AfterViewInit, ChangeDetectorRef, Component, ViewChild, ElementRef, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { RoomService } from './room.service';
import { chatMessageRules, commandRules } from '~validation';
import { NgForm } from '@angular/forms';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { AutoScrollDirection } from './auto-scroll.directive';
import { ResponsiveService } from './responsive.service';
import { Toast } from 'bootstrap';

@Component( {
	selector: 'chat',
	templateUrl: './chat.component.html',
	styleUrls: [ './chat.component.css' ],
	changeDetection: ChangeDetectionStrategy.OnPush
} )
export class ChatComponent implements AfterViewInit, OnInit, OnDestroy {
	constructor(
		private readonly responsiveService: ResponsiveService,
		private readonly roomService: RoomService,
		private readonly changeDetector: ChangeDetectorRef
	) {}

	public ngOnInit() {
		const { destroyed, responsiveService, roomService } = this,
			currentRoomId = roomService.getCurrentRoomId(),
			allMessages = roomService.getMessages();

		responsiveService
		.getBreakpoint()
		.pipe(
			takeUntil( destroyed )
		)
		.subscribe( breakpoint => {
			this.scrollDirection = ( breakpoint === ResponsiveBreakpoint.Xs ) ? AutoScrollDirection.Up : AutoScrollDirection.Down;
			this.changeDetector.markForCheck();
		} );

		currentRoomId.subscribe( roomId => {
			this.roomId = roomId;
		} );
		combineLatest( [ currentRoomId, allMessages ], ( roomId, messages ) =>
			roomId
			? messages.filter( message => message.roomId === roomId )
			: []
		)
		.pipe(
			takeUntil( destroyed )
		)
		.subscribe( messages => {
			this.messages = messages;
			this.recentMessage = messages.slice( -1 )[ 0 ];
			this.changeDetector.markForCheck();
			if( this.canSeeChat || this.recentMessage == null ) {
				this.toast?.hide();
			} else {
				this.toast?.show();
			}
		} );
	}

	public ngAfterViewInit() {
		const { destroyed, responsiveService } = this;

		this.toast = new Toast( this.toastEl.nativeElement, {} );

		responsiveService.getIntersection( this.chatContainer.nativeElement, { root: document } )
		.pipe(
			map( e => e.some( e => e.intersectionRatio > .25 ) ),
			distinctUntilChanged(),
			takeUntil( destroyed )
		)
		.subscribe( canSeeChat => {
			this.canSeeChat = canSeeChat;
			this.changeDetector.markForCheck();
			if( canSeeChat || this.recentMessage == null ) {
				this.toast.hide();
			}
		} );
	}

	public ngOnDestroy() {
		this.destroyed.next( true );
		this.destroyed.complete();
	}

	private readonly destroyed = new Subject<true>();

	@ViewChild( 'chatContainer', { static: false } )
	private chatContainer: ElementRef;

	@ViewChild( 'messageForm', { static: false } )
	private messageForm: NgForm;

	@ViewChild( 'textbox', { static: false } )
	private textbox: ElementRef;

	@ViewChild( 'toast', { static: false } )
	private toastEl: ElementRef;

	private toast: Toast;

	public roomId: string|null;

	public messages = [] as Message[];

	public recentMessage: Message;

	public text: string|void;
	public get isCommand() { return ( this.text || '' ).startsWith( '/' ); }

	public readonly chatMessageRules = chatMessageRules;
	public readonly commandRules = commandRules;

	public canSeeChat = false;
	public scrollDirection = AutoScrollDirection.Down;

	public async sendMessage() {
		const { messageForm, roomService, roomId, text: message, textbox } = this;
		if( !roomId || !message ) return;
		messageForm.resetForm();
		await roomService.sendMessage( roomId, message );
		textbox.nativeElement.focus();
	}
}
