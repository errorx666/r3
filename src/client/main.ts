import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';

import './default.css';
import './polyfills';
import './vendor';

if( process.env.DEBUG !== 'true' ) {
	enableProdMode();
}

platformBrowserDynamic().bootstrapModule( AppModule );
