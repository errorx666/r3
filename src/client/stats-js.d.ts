declare module 'stats-js' {
	export default class Stats {
		public begin();
		public end();
		public showPanel( panel: number );
		public dom: HTMLElement;
	}
}
