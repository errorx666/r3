declare interface WorkerRequestMessageData {
	readonly type: 'request';
	readonly port: MessagePort;
}

declare interface WorkerResponseMessageData {
	readonly type: 'response';
}

declare type WorkerMessageEvent<T extends object> = Omit<MessageEvent, 'data'> & { readonly data: T; };
declare type WorkerMessage = WorkerMessageEvent<WorkerRequestMessageData|WorkerResponseMessageData>;
