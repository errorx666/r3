import { BufferGeometry, Color, Euler, Mesh, Object3D, BooleanKeyframeTrack, KeyframeTrack, NumberKeyframeTrack, InterpolateSmooth, AnimationClip, Vector3, VectorKeyframeTrack, Quaternion, QuaternionKeyframeTrack, SphereGeometry, PlaneGeometry, Material, ShaderMaterial, OrthographicCamera, WebGLRenderer, Scene, AmbientLight, MeshBasicMaterial, LineSegments, Vector2, MeshNormalMaterial, Box3, Texture, DataTexture, RGBAFormat, PixelFormat, LuminanceFormat, UnsignedByteType, UVMapping, ClampToEdgeWrapping, NearestFilter, DisplayP3ColorSpace, ReinhardToneMapping, MeshStandardMaterial, Object3DEventMap } from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { OutputPass } from 'three/examples/jsm/postprocessing/OutputPass';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';

import boardSettings from '~data/board.yaml';
import { WorkerHost } from './worker-host';
import { PieceGeometry } from '~client/geometry/piece-geometry';

import * as idbKeyval from 'idb-keyval';
import { firstValueFrom } from 'rxjs';
import { GammaCorrectionShader } from 'three/examples/jsm/shaders/GammaCorrectionShader';
import { BoardWorkerMessageMap, SerializedBillboard, SerializedBoardMaps, SerializedBorderMaps, SerializedColor, SerializedPieceFaceMaps, SerializedTexture } from './worker-messages';

const USE_CACHE = process.env.NODE_ENV === 'production';
const builderCacheStore = USE_CACHE ? idbKeyval.createStore( 'builder', 'cache' ) : null;

type CanvasBuilder<T = void> =
	( params: {
		c2d: CanvasRenderingContext2D,
		canvas: HTMLCanvasElement
	} ) => T;

const MAP_SIZE = boardSettings.mapSize;

declare class OffscreenCanvas extends HTMLCanvasElement {
	constructor( width: number, height: number );
}

const alphaTest = 1 / 128;

let offscreenCanvas: HTMLCanvasElement = null;
let offscreenRenderer: WebGLRenderer = null;

let useBackgroundRenderer: Promise<boolean>;

let worker: WorkerHost<BoardWorkerMessageMap>;

function logImage( name: string, canvas: HTMLCanvasElement ) {
	if( !IS_WORKER && process.env.DEBUG === 'true' ) {
		try {
			const dest = document.createElement( 'canvas' );
			dest.width = canvas.width;
			dest.height = canvas.height;
			const c2d = dest.getContext( '2d' );
			c2d.drawImage( canvas as any, 0, 0, canvas.width, canvas.height, 0, 0, dest.width, dest.height );
			const url = dest.toDataURL( 'image/png' );
			console.log( name + '\n%c+', 'font-size: 1px; padding: ' + Math.floor( dest.height / 2 ) + 'px ' + Math.floor( dest.width / 2 ) + 'px; line-height: ' + Math.floor( dest.height / 2 ) + 'px; background: url(' + url + ') no-repeat; background-size: ' + dest.width + 'px ' + dest.height + 'px; color: transparent;' );
		} catch( ex ) {
			console.error( ex );
		}
	}
}

if( IS_WORKER || !boardSettings.useBackgroundRenderer ) {
	useBackgroundRenderer = Promise.resolve( false );
} else {
	worker = new WorkerHost( { src: 'workers/board.worker.js' } );
	useBackgroundRenderer = ( async () => {
		try {
			const result = await firstValueFrom( worker.sendRequest( { type: 'ready' } ) );
			return result?.ready === true;
		} catch( ex ) {
			console.error( ex );
			return false;
		}
	} )();
}

export function disposeOffscreenRenderer() {
	offscreenCanvas = null;
	if( offscreenRenderer != null ) {
		offscreenRenderer.dispose();
		offscreenRenderer.domElement = null;
	}
	offscreenRenderer = null;
}

function getOffscreenRenderer() {
	if( offscreenRenderer ) {
		offscreenRenderer.setClearColor( new Color( 0, 0, 0 ), 0 );
		offscreenRenderer.clear();
	} else {
		prepareOffscreenRenderer();
	}
	return offscreenRenderer;
}

function prepareOffscreenRenderer() {
	offscreenCanvas = new OffscreenCanvas( MAP_SIZE, MAP_SIZE );
	offscreenRenderer = new WebGLRenderer( {
		alpha: false,
		antialias: true,
		canvas: offscreenCanvas,
		precision: 'highp',
		stencil: false
	} );
	offscreenRenderer.debug.checkShaderErrors = ( String( process.env.DEBUG ) === 'true' );
	offscreenRenderer.toneMapping = ReinhardToneMapping;
	offscreenRenderer.outputColorSpace = DisplayP3ColorSpace;
}

export function serializeDataTexture( texture: Texture ): SerializedTexture {
	const { name, image, format, type, mapping, wrapS, wrapT, magFilter, minFilter, anisotropy, colorSpace } = texture as any;
	return { buildId: BUILD_ID, name, image, format, type, mapping, wrapS, wrapT, magFilter, minFilter, anisotropy, colorSpace };
}

export function deserializeDataTexture( { name, image: { data, width, height }, format, type, mapping, wrapS, wrapT, magFilter, minFilter, anisotropy, colorSpace }: SerializedTexture ) {
	const tex = new DataTexture( data, width, height, format, type, mapping, wrapS, wrapT, magFilter, minFilter, anisotropy, colorSpace );
	if( name != null ) tex.name = name;
	tex.generateMipmaps = false;
	tex.needsUpdate = true;
	return tex;
}

const lineMaterial = new MeshBasicMaterial( { color: new Color( 1, 0, 0 ) } );
function line( ...points: Point[] ) {
	// const z = 1;
	const geometry = new BufferGeometry;
	// geometry.vertices.push( ...points.map( ( { x, y } ) => new Vector3( x, y, z ) ) );
	return new LineSegments( geometry, lineMaterial );
}

export function lineHoriz( y: number, { width }: Size ) {
	return line( { x: width * -.5, y }, { x: width * .5, y } );
}

export function lineVert( x: number, { height }: Size ) {
	return line( { x, y: height * -.5 }, { x, y: height * .5 } );
}

export function crosshair( { x, y }: Point, size: Size ) {
	return [ lineVert( x, size ), lineHoriz( y, size ) ];
}

export function newCanvas( { width, height }: Size ) {
	return new OffscreenCanvas( width, height );
}

export function buildCanvas( { width, height }: Size, fn: CanvasBuilder ) {
	const canvas = newCanvas( { width, height } );
	const c2d = canvas.getContext( '2d' );
	c2d.imageSmoothingEnabled = true;
	c2d.imageSmoothingQuality = 'high';
	fn( { c2d, canvas } );
	return { c2d, canvas };
}

function buildTextureImpl( name: string, fn: CanvasBuilder, fn2: CanvasBuilder<Texture> ) {
	const { c2d, canvas } = buildCanvas( { width: MAP_SIZE, height: MAP_SIZE }, fn );
	if( process.env.DEBUG === 'true' ) logImage( name, canvas );
	const texture = fn2( { c2d, canvas } );
	texture.name = name;
	return texture;
}

export function buildTextureRGBA( name: string, fn: CanvasBuilder ) {
	return buildTextureImpl( name, fn, ( { c2d } ) => imageDataToTextureRGBA( c2d.getImageData( 0, 0, MAP_SIZE, MAP_SIZE ) ) );
}

export function buildTextureLuminance( name: string, fn: CanvasBuilder ) {
	return buildTextureImpl( name, fn, ( { c2d } ) => imageDataToTextureLuminance( c2d.getImageData( 0, 0, MAP_SIZE, MAP_SIZE ) ) );
}

export function getDescendants( root: Object3D ): ReadonlyArray<Object3D> {
	const objects = [];
	root.traverse( o => { objects.push( o ); } );
	return objects;
}

function materialToImageData( material: Material ) {
	const camera = new OrthographicCamera( MAP_SIZE * -.5, MAP_SIZE * .5, MAP_SIZE * -.5, MAP_SIZE * .5, 0.1, 1 );
	const geom = new PlaneGeometry( MAP_SIZE, MAP_SIZE, 1, 1 );
	const mesh = new Mesh( geom, material );
	const scene = new Scene;
	scene.add( camera );
	scene.add( new AmbientLight( new Color( 1, 1, 1 ), 1 ) );
	scene.add( mesh );
	camera.position.set( 0, 0, -.5 );
	camera.lookAt( mesh.position );
	const renderer = getOffscreenRenderer();
	const composer = new EffectComposer( renderer );
	const renderPass = new RenderPass( scene, camera );
	// const taaRenderPass = new TAARenderPass( scene, camera, 0x000000, 0xffffff );
	// taaRenderPass.sampleLevel = 5;
	// const fxaaPass = new ShaderPass( FXAAShader );
	// fxaaPass.uniforms[ 'resolution' ].value.x = 1 / ( renderer.domElement.width * renderer.getPixelRatio() );
	// fxaaPass.uniforms[ 'resolution' ].value.y = 1 / ( renderer.domElement.height * renderer.getPixelRatio() );
	const gammaCorrectionPass = new ShaderPass( GammaCorrectionShader );
	const outputPass = new OutputPass();
	// composer.addPass( taaRenderPass );
	composer.addPass( renderPass );
	// composer.addPass( fxaaPass );
	composer.addPass( gammaCorrectionPass );
	composer.addPass( outputPass );
	composer.render();
	if( process.env.DEBUG === 'true' ) logImage( material.name, offscreenCanvas );
	material.dispose();
	geom.dispose();
	return getCanvasData( offscreenCanvas );
}

function intensityAsColor( height: number ) {
	const c = Math.round( height * 255 ).toString( 16 ).padStart( 2, '0' );
	return `#${c}${c}${c}`;
}

function rect(
	x: number,
	y: number,
	width: number,
	height: number,
	r1 = 0,
	r2 = r1,
	r3 = r1,
	r4 = r2
) {
	const path = new Path2D;
	path.moveTo( x + r4, y );
	path.lineTo( x + width - r1, y );
	path.quadraticCurveTo( x + width, y, x + width, y + r1 );
	path.lineTo( x + width, y + height - r2 );
	path.quadraticCurveTo( x + width, y + height, x + width - r2, y + height );
	path.lineTo( x + r3, y + height );
	path.quadraticCurveTo( x, y + height, x, y + height - r3 );
	path.lineTo( x, y + r4 );
	path.quadraticCurveTo( x, y, x + r4, y );
	path.closePath();
	return path;
}

const pieceGeometry = ( () => {
	const geom = new PieceGeometry( {
		radius: .4,
		thickness: .125,
		sides: 32
	} );
	geom.name = 'piece_geometry';
	return geom;
} )();

export function buildPieceFaceMaps() {
	const alphaMap = buildTextureRGBA( 'alpha_map', ( { c2d } ) => {
		c2d.fillStyle = intensityAsColor( 1 );
		c2d.arc( MAP_SIZE * .5, MAP_SIZE * .5, MAP_SIZE * .5, 0, Math.PI * 2 );
		c2d.fill();
	} );

	const bumpMap = buildTextureRGBA( 'bump_map', ( { c2d } ) => {
		c2d.clearRect( 0, 0, MAP_SIZE, MAP_SIZE );
		const gradient = c2d.createRadialGradient( MAP_SIZE * .5, MAP_SIZE * .5, 0, MAP_SIZE * .5, MAP_SIZE * .5, MAP_SIZE * .5 );
		gradient.addColorStop( .05, intensityAsColor( .4 ) );
		gradient.addColorStop( .6, intensityAsColor( .4 ) );
		gradient.addColorStop( .8, intensityAsColor( .6 ) );
		gradient.addColorStop( 1, intensityAsColor( 0 ) );
		c2d.fillStyle = gradient;
		c2d.arc( MAP_SIZE * .5, MAP_SIZE * .5, MAP_SIZE * .5, 0, Math.PI * 2 );
		c2d.fill();
	} );

	return { buildId: BUILD_ID, alphaMap, bumpMap };
}

const pieceFaceMaterial = ( async () => {
	let alphaMap: Texture;
	let bumpMap: Texture;
	const CACHE_KEY = 'pieceFaceMaterial';
	let serializedPieceFaceMaterial = USE_CACHE ? await idbKeyval.get( CACHE_KEY, builderCacheStore ) as SerializedPieceFaceMaps : null;
	if( serializedPieceFaceMaterial?.buildId !== BUILD_ID && await useBackgroundRenderer ) {
		serializedPieceFaceMaterial = await firstValueFrom( worker.sendRequest( { type: 'piece-face-maps' } ) );
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, serializedPieceFaceMaterial, builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}
	if( serializedPieceFaceMaterial?.buildId === BUILD_ID ) {
		alphaMap = deserializeDataTexture( serializedPieceFaceMaterial.alphaMap );
		bumpMap = deserializeDataTexture( serializedPieceFaceMaterial.bumpMap );
	} else {
		( { alphaMap, bumpMap } = buildPieceFaceMaps() );
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, {
					buildId: BUILD_ID,
					alphaMap: serializeDataTexture( alphaMap ),
					bumpMap: serializeDataTexture( bumpMap )
				}, builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}
	const material = new MeshStandardMaterial( {
		name: 'piece_face_material',
		alphaTest,
		metalness: .8,
		roughness: .25,
		bumpScale: 2,
		bumpMap,
		fog: false,
		transparent: true,
		alphaMap,
		dithering: true
	} );
	return material;
} )();

const markerMesh = ( async () => {
	const CACHE_KEY = 'markerMesh';
	let serializedBillboard = USE_CACHE ? await idbKeyval.get( CACHE_KEY, builderCacheStore ) as SerializedBillboard : null;
	if( serializedBillboard?.buildId !== BUILD_ID && await useBackgroundRenderer ) {
		serializedBillboard = await firstValueFrom( worker.sendRequest( { type: 'marker' } ) );
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, serializedBillboard, builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}
	if( serializedBillboard?.buildId === BUILD_ID ) {
		return deserializeBillboard( serializedBillboard );
	}
	const markerGeometry = new SphereGeometry( .1, 16, 16 );
	markerGeometry.name = 'marker_geometry';

	const markerMesh = buildBillboard( new Mesh( markerGeometry ) );
	markerMesh.name = 'marker';
	markerMesh.material.name = 'marker_material';
	markerMesh.material.metalness = .7;
	markerMesh.material.roughness = .5;
	markerMesh.material.color = ( new Color ).setRGB( 0, 1, 1 );
	if( USE_CACHE ) {
		try {
			await idbKeyval.set( CACHE_KEY, serializeBillboard( markerMesh ), builderCacheStore );
		} catch( ex ) {
			console?.warn( ex );
		}
	}
	markerGeometry.dispose();
	return markerMesh;
} )();

const borderGeometry = ( () => {
	const g = new PlaneGeometry( 1, 1, 1, 1 ).center();
	g.name = 'border_geometry';
	// g.faceVertexUvs = [ g.faceVertexUvs[ 0 ], g.faceVertexUvs[ 0 ] ];
	// g.uvsNeedUpdate = true;
	return g;
} )();

export function buildBorderMaps() {
	const map = imageDataToTextureRGBA( materialToImageData( new ShaderMaterial( {
		name: 'wood_grain',
		depthTest: false,
		depthWrite: false,
		lights: false,
		fragmentShader: require( '~client/shaders/wood.frag' ),
		vertexShader: require( '~client/shaders/wood.vert' ),
		uniforms: {
			scale: {
				value: 2
			},
			color1: {
				value: ( new Color ).setRGB( 0, 0, 0 )
			},
			color2: {
				value: ( new Color ).setRGB( .5, 0, 0 )
			},
			frequency: {
				value: 7.6
			},
			noiseScale: {
				value: 7
			},
			ringScale: {
				value: .4
			}
		}
	} ) ) );

	const faceMap = buildTextureRGBA( 'border_face', ( { c2d, canvas: { width, height } } ) => {
		c2d.fillStyle = '#000';
		c2d.fillRect( 0, 0, width, height );
		c2d.globalCompositeOperation = 'lighten';

		const borderSize = { x: .1 * width, y: .1 * height };
		c2d.clip( rect( 0, 0, width, height, MAP_SIZE * .1 ) );
		const gradientN = c2d.createLinearGradient( width * .5, 0, width * .5, borderSize.y );
		const gradientE = c2d.createLinearGradient( width, height * .5, width - borderSize.x, height * .5 );
		const gradientS = c2d.createLinearGradient( width * .5, height, width * .5, height - borderSize.y );
		const gradientW = c2d.createLinearGradient( 0, height * .5, borderSize.x, height * .5 );
		for( const gradient of [ gradientN, gradientE, gradientS, gradientW ] ) {
			gradient.addColorStop( 0, intensityAsColor( 0 ) );
			gradient.addColorStop( .1, intensityAsColor( 1 ) );
			gradient.addColorStop( .8, intensityAsColor( 1 ) );
			gradient.addColorStop( 1, intensityAsColor( 0 ) );
			c2d.fillStyle = gradient;
			c2d.fillRect( 0, 0, width, height );
		}
	} );

	const alphaMap = buildTextureRGBA( 'board_alpha', ( { c2d, canvas: { width, height } } ) => {
		c2d.fillStyle = intensityAsColor( 0 );
		c2d.fillRect( 0, 0, width, height );
		c2d.fillStyle = intensityAsColor( 1 );
		c2d.fill( rect( 0, 0, width, height, MAP_SIZE * .1 ) );
		c2d.fillStyle = intensityAsColor( 0 );
		const offset = .085;
		c2d.fill( rect( width * offset, width * offset, width * ( 1 - offset * 2 ), height * ( 1 - offset * 2 ), MAP_SIZE * .025 ) );
	} );

	return { buildId: BUILD_ID, faceMap, alphaMap, map };
}

const borderMaterial = ( async () => {
	let alphaMap: Texture;
	let faceMap: Texture;
	let map: Texture;
	const CACHE_KEY = 'borderMaterial';
	let serializedBorderMaterial = USE_CACHE ? await idbKeyval.get( CACHE_KEY, builderCacheStore ) as SerializedBorderMaps : null;
	if( serializedBorderMaterial?.buildId !== BUILD_ID && await useBackgroundRenderer ) {
		serializedBorderMaterial = await firstValueFrom( worker.sendRequest( { type: 'border-maps' } ) );
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, serializedBorderMaterial, builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}
	if( serializedBorderMaterial?.buildId === BUILD_ID ) {
		alphaMap = deserializeDataTexture( serializedBorderMaterial.alphaMap );
		faceMap = deserializeDataTexture( serializedBorderMaterial.faceMap );
		map = deserializeDataTexture( serializedBorderMaterial.map );
	} else {
		( { alphaMap, faceMap, map } = buildBorderMaps() );
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, {
					buildId: BUILD_ID,
					alphaMap: serializeDataTexture( alphaMap ),
					faceMap: serializeDataTexture( faceMap ),
					map: serializeDataTexture( map )
				}, builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}

	const borderMaterial = new MeshStandardMaterial( {
		name: 'border_material',
		color: new Color( 1, 1, 1 ),
		map,
		alphaTest,
		roughness: .3,
		metalness: .4,
		transparent: true,
		bumpMap: faceMap,
		bumpScale: 2,
		alphaMap,
		fog: false,
		dithering: true
	} );
	return borderMaterial;
} )();


function getCanvasData( src: HTMLCanvasElement ) {
	let c2d = src.getContext( '2d' );
	if( !c2d ) {
		const dest = newCanvas( { width: src.width, height: src.height } );
		c2d = dest.getContext( '2d' );
		c2d.drawImage( src as any as CanvasImageSource, 0, 0 );
	}
	return c2d.getImageData( 0, 0, src.width, src.height );
}

function imageDataToTextureImpl( { width, height, data }: ImageData, channels: number[], format: PixelFormat ) {
	const strideIn = 4;
	const strideOut = channels.length;
	const size = width * height;
	const arr = new Uint8Array( strideOut * size );
	for( let x = 0; x < width; ++x ) {
		for( let y = 0; y < height; ++y ) {
			const offsetIn = ( y * width + x ) * strideIn;
			const offsetOut = ( ( height - y - 1 ) * width + x ) * strideOut;
			for( let channelOut = 0; channelOut < strideOut; ++channelOut ) {
				const channelIn = channels[ channelOut ];
				arr[ offsetOut + channelOut ] = data[ offsetIn + channelIn ];
			}
		}
	}

	const texture = new DataTexture( arr, width, height, format, UnsignedByteType, UVMapping, ClampToEdgeWrapping, ClampToEdgeWrapping, NearestFilter, NearestFilter, 16, DisplayP3ColorSpace );
	texture.generateMipmaps = false;
	texture.needsUpdate = true;
	return texture;
}

function imageDataToTextureLuminance( data: ImageData ) {
	return imageDataToTextureImpl( data, [ 2 ], LuminanceFormat );
}

function imageDataToTextureRGBA( data: ImageData ) {
	return imageDataToTextureImpl( data, [ 0, 1, 2, 3 ], RGBAFormat );
}

function buildBillboardMaps( object: Object3D ) {
	const scene = new Scene;

	const clone = object.clone();
	scene.add( clone );
	clone.position.set( 0, 0, 0 );
	clone.updateMatrixWorld();
	const box = ( new Box3 ).setFromObject( clone );
	const distance = 0.1;
	const depth = box.max.z - box.min.z;
	const camera = new OrthographicCamera( box.min.x, box.max.x, box.max.y, box.min.y, distance, depth + distance );

	function getImageData( material: Material ) {
		clone.traverse( o => {
			if( o instanceof Mesh ) {
				o.material = material;
			}
		} );
		const renderer = getOffscreenRenderer();
		const composer = new EffectComposer( renderer );
		const renderPass = new RenderPass( scene, camera );
		const fxaaPass = new ShaderPass( FXAAShader );
		fxaaPass.uniforms[ 'resolution' ].value.x = 1 / ( renderer.domElement.width * renderer.getPixelRatio() );
		fxaaPass.uniforms[ 'resolution' ].value.y = 1 / ( renderer.domElement.height * renderer.getPixelRatio() );
		composer.addPass( renderPass );
		composer.addPass( fxaaPass );
		composer.render();
		if( process.env.DEBUG === 'true' ) logImage( object.name + ' ' + material.name, offscreenCanvas );
		const imageData = getCanvasData( offscreenCanvas );
		material.dispose();
		return imageData;
	}

	scene.add( camera );

	camera.position.set( 0, 0, box.max.z + distance );
	camera.lookAt( clone.position );

	const normalMap = imageDataToTextureRGBA( getImageData( new MeshNormalMaterial( {
		alphaTest
	} ) ) );

	const alphaMap = imageDataToTextureRGBA( getImageData( new MeshBasicMaterial( {
		color: new Color( 1, 1, 1 ),
		alphaTest,
		depthTest: false,
		depthWrite: false,
		fog: false
	} ) ) );

	return { alphaMap, normalMap };
}

function buildBillboard( object: Object3D ) {
	const box = ( new Box3 ).setFromObject( object );
	const width = box.max.x - box.min.x;
	const height = box.max.y - box.min.y;
	const { alphaMap, normalMap } = buildBillboardMaps( object );
	const geom = new PlaneGeometry( width, height, 1, 1 );
	// geom.faceVertexUvs = [ geom.faceVertexUvs[ 0 ], geom.faceVertexUvs[ 0 ] ];
	// geom.uvsNeedUpdate = true;
	const material = new MeshStandardMaterial( {
		alphaTest,
		normalMap,
		alphaMap,
		fog: false,
		transparent: true,
		dithering: true
	} );
	if( object.name ) {
		material.name = `material_billboard_${object.name}`;
	}
	return new Mesh( geom, material );
}

export function serializeBillboard( mesh: Mesh ): SerializedBillboard {
	const box = ( new Box3 ).setFromObject( mesh );
	const width = box.max.x - box.min.x;
	const height = box.max.y - box.min.y;
	const mat = mesh.material as MeshStandardMaterial;
	return {
		buildId: BUILD_ID,
		name: mat.name,
		color: mat.color.toArray() as unknown as SerializedColor,
		emissive: mat.emissive.toArray() as unknown as SerializedColor,
		emissiveIntensity: mat.emissiveIntensity,
		roughness: mat.roughness,
		metalness: mat.metalness,
		alphaMap: serializeDataTexture( mat.alphaMap ),
		normalMap: serializeDataTexture( mat.normalMap ),
		size: [ width, height ]
	};
}

export function deserializeBillboard( serialized: SerializedBillboard ) {
	const [ width, height ] = serialized.size;
	const geom = new PlaneGeometry( width, height, 1, 1 );
	// geom.faceVertexUvs = [ geom.faceVertexUvs[ 0 ], geom.faceVertexUvs[ 0 ] ];
	// geom.uvsNeedUpdate = true;
	const material = new MeshStandardMaterial( {
		alphaTest,
		color: ( new Color ).setRGB( ...serialized.color ),
		emissive: ( new Color ).setRGB( ...serialized.emissive ),
		emissiveIntensity: serialized.emissiveIntensity,
		roughness: serialized.roughness,
		metalness: serialized.metalness,
		alphaMap: deserializeDataTexture( serialized.alphaMap ),
		normalMap: deserializeDataTexture( serialized.normalMap ),
		fog: false,
		transparent: true,
		dithering: true
	} );
	const mesh = new Mesh( geom, material );
	if( serialized.name != null ) {
		mesh.name = serialized.name;
		material.name = `material_billboard_${serialized.name}`;
	}
	return mesh;
}

const font = ( new FontLoader ).parse( require( '~data/font.json' ) );

const textMap = new Map<string, Mesh<BufferGeometry, MeshStandardMaterial, Object3DEventMap>>();

export async function getText( str: string ) {
	const CACHE_KEY = `text_${str}`;
	let mesh = textMap.get( str );
	if( mesh ) return mesh;
	let serializedBillboard = USE_CACHE ? await idbKeyval.get( CACHE_KEY, builderCacheStore ) as SerializedBillboard : null;
	if( serializedBillboard?.buildId !== BUILD_ID && await useBackgroundRenderer ) {
		serializedBillboard = await firstValueFrom( worker.sendRequest( { type: 'text', symbol: str } ) );
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, serializedBillboard, builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}
	if( serializedBillboard?.buildId === BUILD_ID ) {
		mesh = deserializeBillboard( serializedBillboard );
	} else {
		const geom = new TextGeometry( str, {
			...boardSettings.textGeometry,
			font
		} ).center();
		mesh = new Mesh( geom );
		mesh.name = `text_${str}`;
		mesh = buildBillboard( mesh );
		geom.dispose();
		mesh.material.color = ( new Color ).setHSL( 3 / 18, 1, .5 );
		mesh.material.metalness = 0;
		mesh.material.roughness = 1;
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, serializeBillboard( mesh ), builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}
	textMap.set( str, mesh );
	return mesh;
}

export function buildBoardMaps() {
	return {
		bumpMap: buildTextureRGBA( 'bump_map', ( { c2d } ) => {
			function draw( offset: number, radius: number, height: number ) {
				const o = MAP_SIZE * offset;
				const r = MAP_SIZE * radius;
				c2d.fillStyle = intensityAsColor( height );
				const path = rect( o, o, MAP_SIZE - o * 2, MAP_SIZE - o * 2, r );
				c2d.fill( path );
			}
			c2d.fillStyle = intensityAsColor( 0.2 );
			c2d.fillRect( 0, 0, MAP_SIZE, MAP_SIZE );
			draw( 0.05, 0.1, .1 );
			draw( 0.075, 0.15, .75 );
			draw( 0.1, 0.15, .1 );
		} ),
		metalnessMap: buildTextureRGBA( 'metalness_map', ( { c2d } ) => {
			c2d.fillStyle = intensityAsColor( .5 );
			c2d.fillRect( 0, 0, MAP_SIZE, MAP_SIZE );
			c2d.clip( rect( MAP_SIZE * .1, MAP_SIZE * .1, MAP_SIZE * .8, MAP_SIZE * .8, MAP_SIZE * .1 ) );
			c2d.clearRect( 0, 0, MAP_SIZE, MAP_SIZE );
		} ),
		roughnessMap: buildTextureRGBA( 'roughness_map', ( { c2d } ) => {
			c2d.fillStyle = intensityAsColor( .2 );
			c2d.fill( rect( MAP_SIZE * .2, MAP_SIZE * .2, MAP_SIZE * .6, MAP_SIZE * .6, MAP_SIZE * .1 ) );
		} )
	};
}

const boardMaterial = ( async () => {
	const CACHE_KEY = 'boardMaterial';
	let bumpMap: Texture;
	let metalnessMap: Texture;
	let roughnessMap: Texture;
	let serializedBoardMaterial = USE_CACHE ? await idbKeyval.get( CACHE_KEY, builderCacheStore ) as SerializedBoardMaps : null;
	if( serializedBoardMaterial?.buildId !== BUILD_ID && await useBackgroundRenderer ) {
		serializedBoardMaterial = await firstValueFrom( worker.sendRequest( { type: 'board-maps' } ) );
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, serializedBoardMaterial, builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}
	if( serializedBoardMaterial?.buildId === BUILD_ID ) {
		bumpMap = deserializeDataTexture( serializedBoardMaterial.bumpMap );
		metalnessMap = deserializeDataTexture( serializedBoardMaterial.metalnessMap );
		roughnessMap = deserializeDataTexture( serializedBoardMaterial.roughnessMap );
	} else {
		( { bumpMap, metalnessMap, roughnessMap } = buildBoardMaps() );
		if( USE_CACHE ) {
			try {
				await idbKeyval.set( CACHE_KEY, {
					buildId: BUILD_ID,
					bumpMap: serializeDataTexture( bumpMap ),
					metalnessMap: serializeDataTexture( metalnessMap ),
					roughnessMap: serializeDataTexture( roughnessMap )
				}, builderCacheStore );
			} catch( ex ) {
				console?.warn( ex );
			}
		}
	}

	const material = new MeshStandardMaterial( {
		name: 'board_material',
		color: ( new Color ).setRGB( 0.084, 1, 0.073 ),
		emissive: ( new Color ).setRGB( 0, 0, 0 ),
		metalness: .8,
		metalnessMap,
		roughness: .8,
		roughnessMap,
		bumpMap,
		bumpScale: 1.5,
		dithering: true
	} );
	return material;
} )();

export async function getBoard( { x, y }: Point, { width, height }: Size ) {
	const geometry = new PlaneGeometry( 1, 1, 1, 1 ).center();
	geometry.name = 'board_geometry';
	const boardMesh = new Mesh( geometry, ( await boardMaterial ).clone() );
	return boardMesh;
}

export async function getBorder() {
	const borderMesh = new Mesh( borderGeometry, await borderMaterial );
	borderMesh.name = 'border';
	return borderMesh;
}

export async function getMarker() {
	const mesh = ( await markerMesh ).clone();
	mesh.material = mesh.material.clone();
	mesh.position.set( .25, .25, .75 );
	return mesh;
}

export async function getPiece() {
	const faceMaterials = [] as MeshStandardMaterial[];
	for( let i = 0; i < 2; ++i ) {
		const faceMaterial = ( await pieceFaceMaterial ).clone();
		faceMaterial.name += `_${i + 1}`;
		faceMaterials[ i ] = faceMaterial;
	}
	const pieceMesh = new Mesh(
		pieceGeometry,
		faceMaterials
	);
	pieceMesh.translateZ( 0.1 );
	pieceMesh.receiveShadow = false;
	return pieceMesh;
}

export function getArray<T extends object>( value: T|ArrayLike<T> ): ReadonlyArray<T> {
	return [ ...( function *() {
		if( !value ) return;
		if( typeof ( value as ArrayLike<T> ).length === 'number' ) {
			yield *Array.from( value as ArrayLike<T> );
		} else {
			yield value as T;
		}
	}() ) ];
}

export function disposeMaterials( mesh: Mesh ) {
	for( const material of getArray( mesh.material ) ) {
		if( material ) material.dispose();
	}
}

export function setPieceTopColor( piece: Mesh<BufferGeometry, MeshStandardMaterial[], Object3DEventMap>, color: Color ) {
	piece.material[ 0 ].color.copy( color );
}

export function setPieceBottomColor( piece: Mesh<BufferGeometry, MeshStandardMaterial[], Object3DEventMap>, color: Color ) {
	piece.material[ 1 ].color.copy( color );
}

export function enableShadows( obj: Object3D, enable = true ) {
	if( !boardSettings.shadows.enabled ) enable = false;
	obj.traverse( o => {
		if( o instanceof Mesh ) {
			o.castShadow = enable;
		}
	} );
}

const clipDuration = 1.2;

export function generateFlipClip( obj: Object3D ) {
	const tracks = [
		new QuaternionKeyframeTrack(
			'.quaternion',
			[ 0, clipDuration ],
			[
				...( new Quaternion ).setFromEuler( new Euler( Math.PI * 2, Math.PI, Math.PI * .5 ) ).toArray(),
				...( new Quaternion ).setFromEuler( new Euler( 0, 0, 0 ) ).toArray()
			]
		),
		new VectorKeyframeTrack(
			'.position',
			[ 0, clipDuration * .2, clipDuration * .8, clipDuration ],
			[
				...new Vector3( 0, 0, .1 ).toArray(),
				...new Vector3( 0, 0, .4 ).toArray(),
				...new Vector3( 0, 0, .4 ).toArray(),
				...new Vector3( 0, 0, .1 ).toArray()
			],
			InterpolateSmooth
		)
	] as KeyframeTrack[];
	return new AnimationClip( `${obj.name}_flip`, clipDuration, tracks );
}

export function generateFadeClip( obj: Object3D, visible: boolean, shadow: boolean ) {
	if( !boardSettings.shadows.enabled ) shadow = false;
	const tracks = [
		new BooleanKeyframeTrack( '.visible', [ 0, clipDuration ], [ true, visible ] )
	] as KeyframeTrack[];

	function traverse( o: Object3D, name = '' ) {
		if( o instanceof Mesh ) {
			if( shadow ) {
				tracks.push(
					new BooleanKeyframeTrack( `${name}.castShadow`, [ 0, clipDuration * ( visible ? .4 : .6 ) ], [ !visible, visible ] )
				);
			}
			if( o.material instanceof Material ) {
				tracks.push( new NumberKeyframeTrack( `${name}.material.opacity`, [ 0, clipDuration ], [ visible ? 0 : 1, visible ? 1 : 0 ], InterpolateSmooth ) );
				if( !o.material.transparent ) tracks.push( new BooleanKeyframeTrack( `${name}.material.transparent`, [ 0, clipDuration ], [ true, false ] ) );
			} else {
				for( let i = 0; i < o.material.length; ++i ) {
					tracks.push( new NumberKeyframeTrack( `${name}.material[${i}].opacity`, [ 0, clipDuration ], [ visible ? 0 : 1, visible ? 1 : 0 ], InterpolateSmooth ) );
					if( !o.material[ i ].transparent ) tracks.push( new BooleanKeyframeTrack( `${name}.material[${i}].transparent`, [ 0, clipDuration ], [ true, false ] ) );
				}
			}
		}
		for( const child of o.children ) {
			traverse( child, ( name ? `.${name}` : '' ) + child.name );
		}
	}

	traverse( obj );

	return new AnimationClip( `${obj.name}_fade_${visible ? 'in' : 'out'}`, clipDuration, tracks );
}
