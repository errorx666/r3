import 'bootstrap';
import { addHook } from 'dompurify';

function isTag<K extends keyof HTMLElementTagNameMap>( e: Element, tagName: K ): e is HTMLElementTagNameMap[K] {
	return !!( e && e.tagName && e.tagName.toLowerCase() === tagName );
}

addHook( 'afterSanitizeAttributes', node => {
	if( isTag( node, 'a' ) ) {
		node.rel = 'nofollow noopener';
		node.target = '_blank';
	}
} );
