import { BufferGeometry, Float32BufferAttribute, Vector3 } from 'three';

export interface PieceGeometryParameters {
	readonly radius: number;
	readonly thickness: number;
	readonly sides: number;
}
export class PieceGeometry extends BufferGeometry {
	public readonly parameters: PieceGeometryParameters;

	public readonly type = 'PieceGeometry';

	public constructor( { radius, thickness, sides }: PieceGeometryParameters ) {
		super();
		this.parameters = Object.freeze( { radius, thickness, sides } );

		let indices = [] as number[];
		let vertices = [] as number[];
		let normals = [] as number[];
		let uvs = [] as number[];

		const TAU = 2 * Math.PI;
		const uvScale = 1 / 256;
		const uvOffset = 0.5 - uvScale * 0.5;
		{
			const startVertex = vertices.length / 3;
			for( let iy = 0; iy < 2; ++iy ) {
				const y = ( iy - 0.5 ) * radius * 2;
				for( let ix = 0; ix < 2; ++ix ) {
					const x = ( ix - 0.5 ) * radius * 2;
					vertices = [ ...vertices, x, -y, thickness * 0.5 ];
					normals = [ ...normals, 0, 0, 1 ];
					uvs = [ ...uvs, ix, 1 - iy ];
				}
			}
			const startIndex = indices.length;
			indices = [ ...indices, ...[ 0, 2, 1, 2, 3, 1 ].map( i => i + startVertex ) ];
			const startVertex2 = vertices.length / 3;
			for( let y = 0; y < 2; ++y ) {
				for( let x = 0; x <= sides; ++x ) {
					const u = x / sides;
					const theta = u * TAU;
					const sinTheta = Math.sin( theta );
					const cosTheta = Math.cos( theta );
					const normal = new Vector3( sinTheta, cosTheta, 0 ).normalize();
					vertices = [
						...vertices,
						radius * sinTheta,
						radius * cosTheta,
						y * thickness * .5
					];
					normals = [ ...normals, ...normal.toArray() ];
					// uvs = [ ...uvs, u, 1 - y ];
					uvs = [ ...uvs, u * uvScale + uvOffset, 1 - ( y * uvScale + uvOffset ) ];
				}
			}
			for( let x = 0; x < sides; ++x ) {
				const a = x;
				const b = sides + x + 1;
				const c = sides + x + 2;
				const d = x + 1;
				indices = [ ...indices, ...[ a, b, d, b, c, d ].map( i => i + startVertex2 ) ];
			}
			super.addGroup( startIndex, ( sides + 2 ) * 6, 0 );
		}
		{
			const startIndex = indices.length;
			const startVertex = vertices.length / 3;
			for( let iy = 0; iy < 2; ++iy ) {
				const y = ( iy - 0.5 ) * radius * 2;
				for( let ix = 0; ix < 2; ++ix ) {
					const x = ( ix - 0.5 ) * radius * 2;
					vertices = [ ...vertices, x, -y, thickness * -0.5 ];
					normals = [ ...normals, 0, 0, -1 ];
					uvs = [ ...uvs, 1 - ix, 1 - iy ];
				}
			}
			indices = [ ...indices, ...[ 1, 3, 2, 1, 2, 0 ].map( i => i + startVertex ) ];
			const startVertex2 = vertices.length / 3;
			for( let y = 0; y < 2; ++y ) {
				for( let x = 0; x <= sides; ++x ) {
					const u = x / sides;
					const theta = u * TAU;
					const sinTheta = Math.sin( theta );
					const cosTheta = Math.cos( theta );
					const normal = new Vector3( sinTheta, cosTheta, 0 ).normalize();
					vertices = [
						...vertices,
						radius * sinTheta,
						radius * cosTheta,
						y * thickness * -.5
					];
					normals = [ ...normals, ...normal.toArray() ];
					// uvs = [ ...uvs, u, 1 - y ];
					uvs = [ ...uvs, u * uvScale + uvOffset, y * uvScale + uvOffset ];
				}
			}
			for( let x = 0; x < sides; ++x ) {
				const a = x;
				const b = sides + x + 1;
				const c = sides + x + 2;
				const d = x + 1;
				indices = [ ...indices, ...[ d, c, b, d, b, a ].map( i => i + startVertex2 ) ];
			}
			super.addGroup( startIndex, ( sides + 2 ) * 6, 1 );
		}

		super.setIndex( indices );
		super.setAttribute( 'position', new Float32BufferAttribute( vertices, 3 ) );
		super.setAttribute( 'normal', new Float32BufferAttribute( normals, 3 ) );
		super.setAttribute( 'uv', new Float32BufferAttribute( uvs, 2 ) );
	}
}
