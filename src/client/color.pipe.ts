import { Pipe, PipeTransform } from '@angular/core';

export enum ColorType {
	hex = 'hex',
	hsl = 'hsl',
	rgb = 'rgb'
}

@Pipe( {
	name: 'color'
} )
export class ColorPipe implements PipeTransform {
	public transform( colorData: number[], colorType = ColorType.hsl ) {
		switch( colorType ) {
		case 'hex':
			return `#${colorData.map( c => c.toString( 16 ).padStart( 2, '0' ) ).join( '' )}`;
		case 'hsl': {
			const [ h, s, l, a ] = colorData;
			if( a === undefined ) return `hsl(${h}deg,${s}%,${l}%)`;
			else return `hsla(${h}deg,${s}%,${l}%,${a})`;
		}
		case 'rgb': {
			const [ r, g, b, a ] = colorData;
			if( a === undefined ) return `rgb(${r},${g},${b})`;
			else return `rgba(${r},${g},${b},${a})`;
		}
		default: throw new Error( 'Unknown color type' );
		}
	}
}
