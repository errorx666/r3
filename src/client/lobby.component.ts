import { Component, ViewChild, OnInit, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RoomService } from './room.service';
import { ModalCreateRoomComponent } from './modal/create-room.component';
import { ModalJoinRoomComponent } from './modal/join-room.component';

@Component( {
	selector: 'lobby',
	templateUrl: './lobby.component.html',
	styleUrls: [ './lobby.component.css' ],
	changeDetection: ChangeDetectionStrategy.OnPush
} )
export class LobbyComponent implements OnInit, OnDestroy {
	constructor(
		private readonly changeDetector: ChangeDetectorRef,
		private readonly roomService: RoomService
	) {}

	@ViewChild( 'createRoomModal', { static: false } )
	public createRoomModal: ModalCreateRoomComponent;

	@ViewChild( 'joinRoomModal', { static: false } )
	public joinRoomModal: ModalJoinRoomComponent;

	public joinedRoomIds = [] as string[];

	public ngOnInit() {
		const { destroyed, roomService } = this;
		roomService.getRooms()
		.pipe( takeUntil( destroyed ) )
		.subscribe( rooms => {
			this.rooms = rooms;
			this.changeDetector.markForCheck();
		} );

		roomService.getJoinedRoomIds()
		.pipe( takeUntil( destroyed ) )
		.subscribe( roomIds => {
			this.joinedRoomIds = roomIds;
			this.changeDetector.markForCheck();
		} );
	}

	public ngOnDestroy() {
		this.destroyed.next( true );
		this.destroyed.complete();
	}

	public rooms = [] as ClientRoom[];

	public async joinRoom( room: ClientRoom ) {
		const { joinedRoomIds, roomService } = this;
		if( joinedRoomIds.includes( room.id ) ) {
			await this.showRoom( room );
		} else if( room.hasPassword ) {
			this.joinRoomModal.show( room );
		} else {
			await roomService.joinRoom( room.id, '' );
		}
	}

	public async showRoom( room: ClientRoom ) {
		const { joinedRoomIds, roomService } = this;
		if( joinedRoomIds.includes( room.id ) ) {
			await roomService.setRoom( room.id );
		}
	}

	private readonly destroyed = new Subject<true>();
}
