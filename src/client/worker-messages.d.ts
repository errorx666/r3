import { DataTexture, Mapping, PixelFormat } from 'three';

export interface MessageMap<TKey extends string = string> {
	readonly [ key in TKey ]?: Readonly<Record<'request'|'in'|'out', object>>;
}

export type SerializedColor = readonly [ number, number, number ];

export type SerializedTexture = {
	readonly buildId: string;
	readonly format: PixelFormat;
	readonly mapping: Mapping;
} & Pick<DataTexture, 'name'|'image'|'type'|'wrapS'|'wrapT'|'magFilter'|'minFilter'|'anisotropy'|'colorSpace'>;

export type SerializedBillboard = {
	readonly buildId: string;
	readonly name?: string;
	readonly color: SerializedColor;
	readonly emissive: SerializedColor;
	readonly emissiveIntensity: number;
	readonly roughness: number;
	readonly metalness: number;
	readonly alphaMap: SerializedTexture;
	readonly normalMap: SerializedTexture;
	readonly size: [ number, number ];
};

export type SerializedBoardMaps = {
	readonly buildId: string;
	readonly bumpMap: SerializedTexture;
	readonly metalnessMap: SerializedTexture;
	readonly roughnessMap: SerializedTexture;
};

export type SerializedBorderMaps = {
	readonly buildId: string;
	readonly alphaMap: SerializedTexture;
	readonly faceMap: SerializedTexture;
	readonly map: SerializedTexture;
};

export type SerializedPieceFaceMaps = {
	readonly buildId: string;
	readonly alphaMap: SerializedTexture;
	readonly bumpMap: SerializedTexture;
};

export interface BoardWorkerMessageMap {
	readonly ready: {
		readonly out: { readonly ready: boolean; };
	};
	readonly 'board-maps': {
		readonly out: SerializedBoardMaps;
	};
	readonly 'border-maps': {
		readonly out: SerializedBorderMaps;
	};
	readonly 'marker': {
		readonly out: SerializedBillboard;
	};
	readonly 'piece-face-maps': {
		readonly out: SerializedPieceFaceMaps;
	};
	readonly 'text': {
		readonly request: {
			readonly symbol: string;
		};
		readonly out: SerializedBillboard;
	};
}
