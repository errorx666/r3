import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Injectable, OnDestroy } from '@angular/core';
import { scan, takeUntil } from 'rxjs/operators';
import { toMap } from '~operators';
import { SocketService } from './socket.service';

@Injectable()
export class GameService implements OnDestroy {
	constructor(
		private readonly socketService: SocketService
	) {
		socketService.getMessages<ClientGame>( 'update' )
		.pipe(
			takeUntil( this.destroyed ),
			scan<ClientGame, ClientGame[]>( ( prev, game ) => {
				const games = [ ...prev ];
				const index = games.findIndex( g => g.id === game.id );
				if( index >= 0 ) {
					games.splice( index, 1, game );
				} else {
					games.push( game );
				}
				return games;
			}, [] ),
			toMap( game => game.id )
		).subscribe( this.allGames );
	}

	public ngOnDestroy() {
		this.destroyed.next( true );
		this.destroyed.complete();
	}

	public getGames(): Observable<Map<string, ClientGame>> {
		const { allGames } = this;
		return allGames;
	}

	public async newGame( roomId: string, ruleSet: RuleSet, colors: ReadonlyArray<string>, size: Readonly<Size> ) {
		const { socketService } = this;
		return await socketService.send<ClientGame>( 'newGame', { roomId, ruleSet, colors, size } );
	}

	public async makeMove( roomId: string, position: Readonly<Point> ) {
		const { socketService } = this;
		await socketService.send( 'makeMove', { roomId, position } );
	}

	private readonly destroyed = new Subject<true>();
	private readonly allGames = new BehaviorSubject( new Map<string, ClientGame>() );
}
