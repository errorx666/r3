import { ObjectId } from 'mongodb';
import { Int32 } from 'bson';

declare global {
	export interface MongoExpiration {
		_id: ObjectId;
		expires: Date;
	}

	export interface MongoGame {
		_id: ObjectId;
		colors: string[];
		ruleSet: RuleSet;
		gameStates: MongoGameState[];
	}

	export interface MongoGameState {
		time: Date;
		turn: Int32|null;
		lastMove: Point<Int32>|null;
		size: Size<Int32>;
		data: (boolean|Int32)[];
	}

	export interface MongoRoom {
		_id: ObjectId;
		name: string;
		passwordHash: Binary|null;
		gameId: Binary|null;
	}

	export interface MongoRoomSession {
		roomId: ObjectId;
		sessionId: ObjectId;
		seats: Int32[];
	}

	export interface MongoSession {
		_id: ObjectId;
		nick: string;
		socketId: string;
		userId: Binary|null;
	}

	export interface MongoUser {
		_id: ObjectId;
		nick: string;
		passwordHash: Binary|null;
		roles: string[];
	}
}
