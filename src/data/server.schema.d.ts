import { Binary, ObjectId } from 'mongodb';

declare global {
	export interface ServerExpiration {
		_id: ObjectId;
		expires: Date;
	}

	export interface ServerGame {
		_id: ObjectId;
		colors: string[];
		ruleSet: RuleSet;
		gameStates: ServerGameState[];
	}

	export interface ServerGameState {
		time: Date;
		turn: number|null;
		lastMove: Point|null;
		size: Size;
		data: (boolean|number)[];
	}

	export interface ServerRoom {
		_id: ObjectId;
		name: string;
		passwordHash: Binary|null;
		gameId: ObjectId|null;
	}

	export interface ServerRoomSession {
		roomId: ObjectId;
		sessionId: ObjectId;
		seats: number[];
	}

	export interface ServerSession {
		_id: ObjectId;
		nick: string;
		socketId: string;
		userId: ObjectId|null;
	}

	export interface ServerUser {
		_id: ObjectId;
		nick: string;
		passwordHash: Binary|null;
		roles: string[];
	}
}
