declare module '~data/*.yaml';

declare type NumberTriple = [ number, number, number ];

declare module '~data/colors.yaml' {
	export interface ColorDefinition {
		readonly key: string;
		readonly displayName: string;
		readonly color: NumberTriple;
	}

	export const colors: { [ name: string ]: ColorDefinition };
}

declare module '~data/enums.yaml' {
	export const ruleSets: string[];
}

declare module '~data/validation.config.yaml' {
	export const nick: StringValidationRules;
	export const roomName: StringValidationRules;
	export const roomPassword: StringValidationRules;
	export const userPassword: StringValidationRules;
	export const chatMessage: StringValidationRules;
	export const command: StringValidationRules;
}
